package com.robekabyle.robekabylerestful.services.interfaces;

import java.util.ArrayList;

import com.robekabyle.robekabylerestful.model.PostComments;

public interface IServiceComments {
	
	public String					addNewComment(String idImage,PostComments comment);
	public String					updateComment(String idImage,PostComments comment);
	public String					deleteComment(String idImage,String commentID);
	public ArrayList<PostComments>	getCommentsByUser(String userID); // for administrator
}
