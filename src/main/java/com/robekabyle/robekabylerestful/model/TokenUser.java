package com.robekabyle.robekabylerestful.model;

public class TokenUser {
	
	private String userId;
	private String userToken;

	public TokenUser() {
		super();
	}

	public TokenUser(String userId, String userToken) {
		super();
		this.userId = userId;
		this.userToken = userToken;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserToken() {
		return userToken;
	}

	public void setUserToken(String userToken) {
		this.userToken = userToken;
	}

}
