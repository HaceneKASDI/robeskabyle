package com.robekabyle.robekabylerestful.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "com.robekabyle.robekabylerestful")

public class ApplicationContextConfig {

}
