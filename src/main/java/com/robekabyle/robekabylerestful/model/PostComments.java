package com.robekabyle.robekabylerestful.model;

import com.robekabyle.robekabylerestful.utils.UtilsFactory;

/**
 * This class represent the comments pushed by users under image post or reply for
 * a post comment, it contains the information like profile of the author, text of the 
 * comment and the date.
 * 
 * @author Acene
 * @version 14-12-2018
 * 
 * 
 */
public class PostComments {
	
	private String _id;
	private String idProfile;
	private String textComment;
	private String dateComment;
	private String profileImage;
	private String fullName;
	
	public PostComments() {
		super();
	}

	public PostComments(String idProfile, String textComment, String dateComment) {
		super();
		this._id 			= UtilsFactory.getUUID();
		this.idProfile 		= idProfile;
		this.textComment 	= textComment;
		this.dateComment 	= UtilsFactory.getCurrentDate();
	}
	
	public PostComments(String idProfile, String textComment,String fullName,String profileImage, String dateComment) {
		super();
		this._id 			= UtilsFactory.getUUID();
		this.idProfile 		= idProfile;
		this.textComment 	= textComment;
		this.fullName 		= fullName;
		this.profileImage	= profileImage;
		this.dateComment 	= UtilsFactory.getCurrentDate();
	}

	public String get_id() {
		return _id;
	}
	public void set_id(String _id) {
		this._id = _id;
	}
	public String getIdProfile() {
		return idProfile;
	}
	public void setIdProfile(String idProfile) {
		this.idProfile = idProfile;
	}
	public String getTextComment() {
		return textComment;
	}
	public void setTextComment(String textComment) {
		this.textComment = textComment;
	}
	public String getDateComment() {
		return dateComment;
	}
	public void setDateComment(String dateComment) {
		this.dateComment = dateComment;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	
	public String getProfileImage() {
		return profileImage;
	}

	public void setProfileImage(String profileImage) {
		this.profileImage = profileImage;
	}

	@Override
	public String toString() {
		return "PostComments [_id=" + _id + ", idProfile=" + idProfile + ", textComment=" + textComment
				+ ", dateComment=" + dateComment + ", fullName=" + fullName + "]";
	}
	

	
}
