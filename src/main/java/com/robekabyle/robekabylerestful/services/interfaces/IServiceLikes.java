package com.robekabyle.robekabylerestful.services.interfaces;

import java.util.ArrayList;

import com.robekabyle.robekabylerestful.model.PostComments;

public interface IServiceLikes {

	public String					addNewLike(String idImage,String profileID);
	public String					deleteLike(String idImage,String profileID);
	public ArrayList<PostComments>	getLikesByUser(String profileID); // for administrator
}
