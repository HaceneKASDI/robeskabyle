package com.robekabyle.robekabylerestful.controller;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class MailService {

	private static final String MAIL_ADMIN 		= "admin@robes-kabyles.org";
	private static final String MAIL_CONTACT 	= "contact@robes-kabyles.org";
	private static final String MAIL_PASS 		= "Syphax2012+";

	public MailService() {

	}

	public void sendMailRobesKabylesOrg(String toAddress, String fromAddress, String subject, String msgBody) {
		try {
			Properties props = System.getProperties();
			props.put("mail.smtp.port", "587");
			props.put("mail.transport.protocol", "smtp");
			props.put("mail.smtp.host", "smtp.ionos.fr");
			props.put("mail.smtp.auth", true);
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.ssl.trust", "smtp.ionos.fr");

			Session session = Session.getInstance(props, new Authenticator() {
				@Override
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(MailService.MAIL_CONTACT, MailService.MAIL_PASS);
				}
			});

			MimeMessage mimeHelper = new MimeMessage(session);
			mimeHelper.addRecipient(Message.RecipientType.TO, new InternetAddress(MailService.MAIL_ADMIN));
			mimeHelper.setSubject(subject);
			mimeHelper.setFrom(new InternetAddress(MailService.MAIL_CONTACT));
			mimeHelper.setText(msgBody);

			Transport transport = session.getTransport();
			// Send the message.
			try {
				System.out.println("Sending...");

				// Connect to Amazon SES using the SMTP username and password you specified
				// above.
				transport.connect("smtp.ionos.fr", MailService.MAIL_CONTACT, MailService.MAIL_PASS);

				// Send the email.
				Transport.send(mimeHelper);
				System.out.println("Email sent!");
			} catch (Exception ex) {
				System.out.println("The email was not sent.");
				System.out.println("Error message: " + ex.getMessage());
			} finally {
				// Close and terminate the connection.
				transport.close();
			}

		} catch (MessagingException mex) {
			mex.printStackTrace();
		}

	}
	
	public void recoverPassword(String toAddress, String subject, String msgBody) {
		try {
			Properties props = System.getProperties();
			props.put("mail.smtp.port", "587");
			props.put("mail.transport.protocol", "smtp");
			props.put("mail.smtp.host", "smtp.ionos.fr");
			props.put("mail.smtp.auth", true);
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.ssl.trust", "smtp.ionos.fr");

			Session session = Session.getInstance(props, new Authenticator() {
				@Override
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(MailService.MAIL_ADMIN, MailService.MAIL_PASS);
				}
			});

			MimeMessage mimeHelper = new MimeMessage(session);
			mimeHelper.addRecipient(Message.RecipientType.TO, new InternetAddress(toAddress));
			mimeHelper.setSubject(subject);
			mimeHelper.setFrom(new InternetAddress(MailService.MAIL_ADMIN));
			mimeHelper.setText(msgBody);

			Transport transport = session.getTransport();
			// Send the message.
			try {
				System.out.println("Sending...");

				// Connect to Amazon SES using the SMTP username and password you specified
				// above.
				transport.connect("smtp.ionos.fr", MailService.MAIL_ADMIN, MailService.MAIL_PASS);

				// Send the email.
				Transport.send(mimeHelper);
				System.out.println("Email sent!");
			} catch (Exception ex) {
				System.out.println("The email was not sent.");
				System.out.println("Error message: " + ex.getMessage());
			} finally {
				// Close and terminate the connection.
				transport.close();
			}

		} catch (MessagingException mex) {
			mex.printStackTrace();
		}

	}

}
