package com.robekabyle.robekabylerestful.model;

import com.robekabyle.robekabylerestful.utils.UtilsFactory;

/**
 * 
 * @author Hacene 
 * @version 06-01-2019
 *
 */
public class Filter {
	
	private String idFilter;
	private String modalName;
	private String modal;
	private int price;
	
	public Filter() {
		super();
	}

	public Filter(String modalName, String modal, int price) {
		super();
		this.idFilter 		= UtilsFactory.getUUID();
		this.modalName 		= modalName;
		this.modal 			= modal;
		this.price 			= price;
	}

	public String getIdFilter() {
		return idFilter;
	}

	public void setIdFilter(String idFilter) {
		this.idFilter = idFilter;
	}

	public String getModalName() {
		return modalName;
	}

	public void setModalName(String modalName) {
		this.modalName = modalName;
	}

	public String getModal() {
		return modal;
	}

	public void setModal(String modal) {
		this.modal = modal;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "Filter [idFilter=" + idFilter + ", modalName=" + modalName + ", modal=" + modal + ", price=" + price
				+ "]";
	}
	
	
	
	

}
