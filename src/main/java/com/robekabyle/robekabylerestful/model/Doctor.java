package com.robekabyle.robekabylerestful.model;

public class Doctor {

	private String docNo;
	private String docName;
	private String docPhone;
	
	public Doctor(String docNo, String docName, String docPhone) {
		super();
		this.docNo = docNo;
		this.docName = docName;
		this.docPhone = docPhone;
	}
	public Doctor() {
		super();
	}
	
	public void setDocNo(String docNo) {
		this.docNo = docNo;
	}
	public void setDocName(String docName) {
		this.docName = docName;
	}
	public void setDocPhone(String docPhone) {
		this.docPhone = docPhone;
	}
	public String getDocNo() {
		return docNo;
	}
	public String getDocName() {
		return docName;
	}
	public String getDocPhone() {
		return docPhone;
	}

	
}
