package com.robekabyle.robekabylerestful.controller;

import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.robekabyle.robekabylerestful.services.implems.ServiceConnection;
import com.robekabyle.robekabylerestful.services.implems.ServiceImages;
import com.robekabyle.robekabylerestful.utils.ErrorProvider;

/**
 * 
 * @author Acene 18-12-2018
 * @version 2018
 * 
 */
@RestController
public class LikeController {

	
	@RequestMapping(value="/PostLike/addLike",method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE+"; charset:utf-8")
	@ResponseBody
	public ResponseEntity<String> addLike( @RequestBody String data)
	{
		/** data = {"idImage" : "", "profileID" : ""} */
		try {
			// CHECK IF IMAGE EXITS
			JSONObject json 	= new JSONObject(data);
			String idImage 		= json.optString("idImage");
			String profileID 	= json.optString("profileID");
			
			ServiceImages serviceImages = new ServiceImages();
			if (serviceImages.getImageByID(idImage).get_id() != null) {
				
				// CHECK IF USER EXISTS AND CONNECTED
				ServiceConnection serviceConnect = new ServiceConnection();
				if(serviceConnect.isUserConnected(profileID)) {
					
				//ADD LIKE TO THE RELATED IMAGE
				serviceImages.addNewLike(idImage, profileID);
				
				// CLOSE CONNECTIONS
				serviceConnect.closeConnection();
				serviceImages.closeConnection();
				return new ResponseEntity<String>(ErrorProvider.LIKE_ADDED, HttpStatus.OK);
				}else {
					return new ResponseEntity<String>(ErrorProvider.USER_DISCONNECTED, HttpStatus.NOT_FOUND);
				}
			}else {
				return new ResponseEntity<String>(ErrorProvider.LIKE_ERROR, HttpStatus.NOT_FOUND);				
			}
			
		} catch (Exception e) {
			return new ResponseEntity<String>(ErrorProvider.BAD_MANIPULATION, HttpStatus.REQUESTED_RANGE_NOT_SATISFIABLE);
		}
	}

	@RequestMapping(value="/PostLike/deleteLike",method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE+"; charset:utf-8")
	@ResponseBody
	public ResponseEntity<String> deleteLike( @RequestBody String data)
	{
		/** data = {"idImage" : "", "profileID"} */
		try {
			// CHECK IF IMAGE EXITS
			JSONObject json 	= new JSONObject(data);
			String idImage 		= json.optString("idImage");
			String profileID 	= json.optString("profileID");
			
			ServiceImages serviceImages = new ServiceImages();
			if (serviceImages.getImageByID(idImage).get_id() != null) {
				
				// CHECK IF USER EXISTS AND CONNECTED
				ServiceConnection serviceConnect = new ServiceConnection();
				if(serviceConnect.isUserConnected(profileID)) {
					
				//DELETE LIKE RELATED TO IMAGE
				serviceImages.deleteLike(idImage, profileID);
				
				// CLOSE CONNECTIONS
				serviceConnect.closeConnection();
				serviceImages.closeConnection();
				return new ResponseEntity<String>(ErrorProvider.LIKE_DELETED, HttpStatus.OK);
				}else {
					return new ResponseEntity<String>(ErrorProvider.USER_DISCONNECTED, HttpStatus.NOT_FOUND);
				}
			}else {
				return new ResponseEntity<String>(ErrorProvider.LIKE_ERROR, HttpStatus.NOT_FOUND);				
			}
			
		} catch (Exception e) {
			return new ResponseEntity<String>(ErrorProvider.BAD_MANIPULATION, HttpStatus.REQUESTED_RANGE_NOT_SATISFIABLE);
		}
	}
	
}
