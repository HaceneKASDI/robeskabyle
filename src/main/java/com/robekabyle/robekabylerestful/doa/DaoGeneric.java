package com.robekabyle.robekabylerestful.doa;

import org.mongojack.JacksonDBCollection;

import com.robekabyle.robekabylerestful.utils.MongodbConnector;

public class DaoGeneric<T> {
	private MongodbConnector mongoConnector ;
	
	public DaoGeneric() {
		mongoConnector = new MongodbConnector();
	}

	public JacksonDBCollection<T, String> getCollection(String collectionName, Class<T> className) {
		JacksonDBCollection<T, String> collection = 
					JacksonDBCollection.wrap(mongoConnector.getCollectionByName(collectionName), className, String.class);
			return collection;
			
	}
	
	public void closeConnection() {
		mongoConnector.closeConnection();
	}
}
