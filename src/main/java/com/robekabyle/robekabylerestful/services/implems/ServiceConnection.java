package com.robekabyle.robekabylerestful.services.implems;

import java.util.List;

import com.robekabyle.robekabylerestful.doa.implems.DaoConnectionToken;
import com.robekabyle.robekabylerestful.doa.interfaces.IDaoConnectionToken;
import com.robekabyle.robekabylerestful.model.ConnectionToken;
import com.robekabyle.robekabylerestful.services.interfaces.IServiceConnection;

public class ServiceConnection implements IServiceConnection {

	
	private IDaoConnectionToken daoConnection ;
	
	public ServiceConnection() {
		this.daoConnection = new DaoConnectionToken(); 
	}
	
	public List<ConnectionToken> getAllConnections() {
		return daoConnection.getAll();
	}

	public String saveNewConnection(ConnectionToken entity) {
		return daoConnection.save(entity);
	}

	public String updateConnection(ConnectionToken entity) {
		return daoConnection.update(entity);
	}

	public ConnectionToken getConnectionByUsername(String username) {
		return daoConnection.findConnectionByUsername(username);
	}

	public ConnectionToken authenticateUser(String username,String password) {
		return daoConnection.findAutheticatedUser(username, password) ;
	}

	public ConnectionToken getConnectionByFacebookID(String facebookID) {
		return daoConnection.findConnectionByFacebookID(facebookID);
	}

	public String logoutUser(ConnectionToken entity) {
		entity.setIsConnected(false);
		return daoConnection.update(entity);
	}

	@Override
	public boolean isUserConnected(String userProfile) {
		return daoConnection.IsAuthenticated(userProfile);
	}

	@Override
	public void closeConnection() {
		daoConnection.closeConnection();
	}

}
