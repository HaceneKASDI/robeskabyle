package com.robekabyle.robekabylerestful.services.implems;

import java.util.ArrayList;
import java.util.List;

import com.robekabyle.robekabylerestful.doa.implems.DaoImage;
import com.robekabyle.robekabylerestful.doa.interfaces.IDaoImage;
import com.robekabyle.robekabylerestful.model.Badge;
import com.robekabyle.robekabylerestful.model.Filter;
import com.robekabyle.robekabylerestful.model.ImagePost;
import com.robekabyle.robekabylerestful.model.PostComments;
import com.robekabyle.robekabylerestful.services.interfaces.IServiceComments;
import com.robekabyle.robekabylerestful.services.interfaces.IServiceImages;
import com.robekabyle.robekabylerestful.services.interfaces.IServiceLikes;
import com.robekabyle.robekabylerestful.utils.ErrorProvider;
import com.robekabyle.robekabylerestful.utils.UtilsFactory;

public class ServiceImages implements 	IServiceImages,
										IServiceComments,
										IServiceLikes{

	private IDaoImage daoImage ;
	
	public ServiceImages() {
		daoImage = new DaoImage();
	}
	
	//============================================
	//				IMAGES SECTION
	//============================================
	
	public List<ImagePost> getAllImages() {
		return daoImage.getAll();
	}

	public String saveNewImage(ImagePost image) {
		return daoImage.save(image);
	}

	public String updateImage(ImagePost image) {
		return daoImage.update(image);
	}

	public List<ImagePost> getImagesByModal(String modalDress) {
		return daoImage.searchModal(modalDress);
	}

	
	/**
	 * This method return the Images posted by the given user Profile
	 */
	public List<ImagePost> getImagesByUserID(String userID) {
		return daoImage.searchByUserID(userID);
	}

	/**
	 * This methods returns List<ImagePost> which contains the given token in the modelDress.model
	 * or in model.description or in the textPost
	 */
	public List<ImagePost> searchImagesByToken(String searchToken) {
		return daoImage.searchByToken(searchToken);
	}


	@Override
	public List<ImagePost> searchImagesByFilter(Filter filter) {
		return daoImage.searchByFilter(filter);
	}
	
	/**
	 * this methods delete the ImagePost
	 */
	public String deleteImage(String imageID) {
		return daoImage.delete(imageID);
	}


	public ImagePost getImageByID(String _id) {
		return daoImage.get(_id);
	}
	
	@Override
	public String updateAuthorImage(String userId,String urlProfile) {
		try {
			List<ImagePost> imagesUser= daoImage.searchByUserID(userId);
			imagesUser.stream().forEach(image -> image.setUrlProfile(urlProfile));
			imagesUser.stream().forEach(image -> daoImage.update(image));
			return ErrorProvider.OK_MONGODB;
		} catch (Exception e) {
			return ErrorProvider.ERROR_UPDATING_MONGODB;
		}
	}

	//============================================
	//				COMMENTS SECTION
	//============================================
	
	public String addNewComment(String idImage, PostComments comment) {
		ImagePost image = this.getImageByID(idImage);
		ArrayList<PostComments> listComments = image.getComments();
		comment.set_id(UtilsFactory.getUUID());
		comment.setDateComment(UtilsFactory.getCurrentDate());
		listComments.add(comment);
		image.setComments(listComments);
		return daoImage.update(image);
	}

	public String updateComment(String idImage, PostComments comment) {
		ImagePost image = this.getImageByID(idImage);
		List<PostComments> listComments = image.getComments();
		PostComments updatedComment = 
				listComments.stream()
				.filter(comm -> comment.get_id().equals(comm.get_id()))
				.findFirst()
				.get();
		
		int index = listComments.indexOf(updatedComment);
		listComments.remove(index);
		comment.setDateComment(UtilsFactory.getCurrentDate());
		listComments.add(index, comment);
		image.setComments((ArrayList<PostComments>) listComments);
		return daoImage.update(image);
	}

	public String deleteComment(String idImage, String commentID) {
		ImagePost image = this.getImageByID(idImage);
		ArrayList<PostComments> listComments = image.getComments();
		PostComments deletedComment = 
				listComments.stream()
				.filter(comm -> commentID.equals(comm.get_id()))
				.findFirst()
				.get();
		
		listComments.remove(deletedComment);
		image.setComments(listComments);
		return daoImage.update(image);
	}


	//============================================
	//			LIKES SECTION
	//============================================
	
	public ArrayList<PostComments> getCommentsByUser(String userID) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public String addNewLike(String idImage, String profileID) {
		ImagePost image = this.getImageByID(idImage);
		ArrayList<String> listLikes = image.getLikes();
		if(!listLikes.contains(profileID)) {
				listLikes.add(profileID);
				this.updateBadgeUser(image.getIdProfile(),profileID);
				image.setLikes(listLikes);
				return daoImage.update(image);
		}return ErrorProvider.LIKE_EXISTS;
	}

	@Override
	public String deleteLike(String idImage, String profileID) {
		ImagePost image = this.getImageByID(idImage);
		ArrayList<String> listLikes = image.getLikes();
		String deletedLike = 
				listLikes.stream()
				.filter(like -> profileID.equals(like))
				.findFirst()
				.get();
		
		if(listLikes.contains(deletedLike)) {
			this.deleteLikeUser(image.getIdProfile(),profileID);
			listLikes.remove(deletedLike);
			image.setLikes(listLikes);
			return daoImage.update(image);
		}return ErrorProvider.USER_NOT_FOUND;
	}

	@Override
	public ArrayList<PostComments> getLikesByUser(String profileID) {
		return null;
	}

	@Override
	public void closeConnection() {
		daoImage.closeConnection();
	}

	/**
	 * Update the badge of user
	 * @param profileID
	 */
	private void updateBadgeUser(String profileID,String liker) {
		ServiceBadge serviceBadge = new ServiceBadge();
		Badge badge = serviceBadge.getProfileByUserID(profileID);
		List<String> likes = badge.getLikes();
		likes.add(liker);
		badge.setLikes(likes);
		// GET NUMBER OF LIKES
		int nbrLikes = likes.size();
		if(nbrLikes == 1000) badge.setUserBadge(2); // PROFESSIONNAL
		
		// GET NUMBER OF FOLLOWER
		int nbrFollowers = badge.getFollowers().size();
		if(nbrFollowers == 500) badge.setUserBadge(3);	// EXPERT
		
		serviceBadge.updateBadge(badge);
		serviceBadge.closeConnection();
	}
	
	private void deleteLikeUser(String profileID,String liker) {

		ServiceBadge serviceBadge = new ServiceBadge();
		Badge badge = serviceBadge.getProfileByUserID(profileID);
		List<String> likes = badge.getLikes();
		likes.remove(liker);
		badge.setLikes(likes);
		
		// GET NUMBER OF LIKES
		int nbrLikes = likes.size();
		if(nbrLikes == 1000) badge.setUserBadge(2); // PROFESSIONNAL
		
		// GET NUMBER OF FOLLOWER
		int nbrFollowers = badge.getFollowers().size();
		if(nbrFollowers == 500) badge.setUserBadge(3);	// EXPERT
		
		serviceBadge.updateBadge(badge);
		serviceBadge.closeConnection();	
	}



}
