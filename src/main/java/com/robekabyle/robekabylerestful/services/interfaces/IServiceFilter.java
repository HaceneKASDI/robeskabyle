package com.robekabyle.robekabylerestful.services.interfaces;

import com.robekabyle.robekabylerestful.model.Filter;

public interface IServiceFilter {
	
	public String saveNewFilter(String badgeId, Filter filter);
	public String deleteFilter(String badgeId, String idFilter);
	public void closeConnection();

}
