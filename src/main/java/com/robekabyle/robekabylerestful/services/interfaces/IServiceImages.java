package com.robekabyle.robekabylerestful.services.interfaces;

import java.util.List;

import com.robekabyle.robekabylerestful.model.Filter;
import com.robekabyle.robekabylerestful.model.ImagePost;

public interface IServiceImages {

	public List<ImagePost> 			getAllImages();
	public String 					saveNewImage(ImagePost image);
	public String 					updateImage(ImagePost image);
	public ImagePost				getImageByID(String _id);
	public List<ImagePost>		 	getImagesByModal(String modalDress);
	public List<ImagePost>		 	getImagesByUserID(String userID);
	public List<ImagePost>		 	searchImagesByToken(String searchToken);
	public List<ImagePost>		 	searchImagesByFilter(Filter filter);
	public String					deleteImage(String imageID);
	public String					updateAuthorImage(String userId,String urlProfile);
	public void						closeConnection();
		
}
