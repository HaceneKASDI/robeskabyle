package com.robekabyle.robekabylerestful.controller;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.robekabyle.robekabylerestful.model.Filter;
import com.robekabyle.robekabylerestful.model.ImagePost;
import com.robekabyle.robekabylerestful.services.implems.ServiceConnection;
import com.robekabyle.robekabylerestful.services.implems.ServiceImages;

/**
 * 
 * @author Acene 19-12-2018
 * @version 2018
 * 
 * This class seeks the data and answers the searching requests
 * The methods invoked that we can find here are : 
 * getImageByModal, getImageByUserID, getImageByToken, getImageByID
 */
@RestController
public class SearchController {


	@RequestMapping(value="/SearchImage/getImageByModal",method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE+"; charset:utf-8")
	@ResponseBody
	public ResponseEntity<List<ImagePost>> getImageByModal(@RequestParam String profileID, @RequestBody String modalDress)
	{
		try {

			// CHECK IF USER EXISTS AND CONNECTED
			ServiceConnection serviceConnect = new ServiceConnection();
			if(serviceConnect.isUserConnected(profileID)) {
				
			ServiceImages serviceImages = new ServiceImages();
			JSONObject json = new JSONObject(modalDress);
			List<ImagePost> listImages = serviceImages.getImagesByModal(json.getString(modalDress));
			
			// CLOSE CONNECTIONS
			serviceConnect.closeConnection();
			serviceImages.closeConnection();
				return new ResponseEntity<List<ImagePost>>(listImages, HttpStatus.OK);
			}else {
				// CLOSE CONNECTIONS
				serviceConnect.closeConnection();
				return new ResponseEntity<List<ImagePost>>(new ArrayList<ImagePost>(), HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			System.out.println("#### GET IMAGES MODAL : "+e);
			return new ResponseEntity<List<ImagePost>>(new ArrayList<ImagePost>(), HttpStatus.REQUESTED_RANGE_NOT_SATISFIABLE);
		}
	}
	
	@RequestMapping(value="/SearchImage/getImageById",method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE+"; charset:utf-8")
	@ResponseBody
	public ResponseEntity<ImagePost> getImageById(@RequestParam String profileID, @RequestBody String idImage)
	{
		try {

			// CHECK IF USER EXISTS AND CONNECTED
			ServiceConnection serviceConnect = new ServiceConnection();
			if(serviceConnect.isUserConnected(profileID)) {
				
			ServiceImages serviceImages = new ServiceImages();
			JSONObject json = new JSONObject(idImage);
			ImagePost postImage = serviceImages.getImageByID(json.getString("idImage"));
			// CLOSE CONNECTIONS
			serviceConnect.closeConnection();
			serviceImages.closeConnection();
				return new ResponseEntity<ImagePost>(postImage, HttpStatus.OK);
			}else {
				// CLOSE CONNECTIONS
				serviceConnect.closeConnection();
				return new ResponseEntity<ImagePost>(new ImagePost(), HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			System.out.println("#### GET IMAGE BY ID : "+e);
			return new ResponseEntity<ImagePost>(new ImagePost(), HttpStatus.REQUESTED_RANGE_NOT_SATISFIABLE);
		}
	}
	
	
	@RequestMapping(value="/SearchImage/getImageByToken",method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE+"; charset:utf-8")
	@ResponseBody
	public ResponseEntity<List<ImagePost>> getImageByToken(@RequestParam String profileID, @RequestBody String token)
	{
		try {

			// CHECK IF USER EXISTS AND CONNECTED
			ServiceConnection serviceConnect = new ServiceConnection();
			if(serviceConnect.isUserConnected(profileID)) {
				
			ServiceImages serviceImages = new ServiceImages();
			JSONObject json = new JSONObject(token);
			List<ImagePost> listImages = serviceImages.searchImagesByToken(json.getString("token"));
			// CLOSE CONNECTIONS
			serviceConnect.closeConnection();
			serviceImages.closeConnection();
			return new ResponseEntity<List<ImagePost>>(listImages, HttpStatus.OK);
		}else {
			// CLOSE CONNECTIONS
			serviceConnect.closeConnection();
			return new ResponseEntity<List<ImagePost>>(new ArrayList<ImagePost>(), HttpStatus.NOT_FOUND);
		}
		} catch (Exception e) {
			System.out.println("#### GET IMAGE BY ID : "+e);
			return new ResponseEntity<List<ImagePost>>(new ArrayList<ImagePost>(), HttpStatus.REQUESTED_RANGE_NOT_SATISFIABLE);
		}
	}
	
	
	@RequestMapping(value="/SearchImage/getImageByFilter",method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE+"; charset:utf-8")
	@ResponseBody
	public ResponseEntity<List<ImagePost>> getImageByFilter(@RequestBody Filter filter)
	{
		try {
			/** { "modalName" : "" , "modal" : "" , "price" : ""} */
			ServiceImages serviceImages = new ServiceImages();
			List<ImagePost> listImages = serviceImages.searchImagesByFilter(filter);
			
			// CLOSE CONNECTIONS
			serviceImages.closeConnection();
			return new ResponseEntity<List<ImagePost>>(listImages, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println("#### GET IMAGE BY FILTER : "+e);
			return new ResponseEntity<List<ImagePost>>(new ArrayList<ImagePost>(), HttpStatus.REQUESTED_RANGE_NOT_SATISFIABLE);
		}
	}
	
	@RequestMapping(value="/SearchImage/getImagesByProfile",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<List<ImagePost>> getImagesByProfile(@RequestParam String profileID)
	{
		try {

			// CHECK IF USER EXISTS AND CONNECTED
			ServiceConnection serviceConnect = new ServiceConnection();
			if(serviceConnect.isUserConnected(profileID)) {
				
			ServiceImages serviceImages = new ServiceImages();
			List<ImagePost> listImages = serviceImages.getImagesByUserID(profileID);
			// CLOSE CONNECTIONS
			serviceConnect.closeConnection();
			serviceImages.closeConnection();
				return new ResponseEntity<List<ImagePost>>(listImages, HttpStatus.OK);
			}else {
				// CLOSE CONNECTIONS
				serviceConnect.closeConnection();
				return new ResponseEntity<List<ImagePost>>(new ArrayList<ImagePost>(), HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			System.out.println("#### GET IMAGES MODAL : "+e);
			return new ResponseEntity<List<ImagePost>>(new ArrayList<ImagePost>(), HttpStatus.REQUESTED_RANGE_NOT_SATISFIABLE);
		}
	}
	

}
