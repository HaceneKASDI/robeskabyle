package com.robekabyle.robekabylerestful.model;

import org.mongojack.Id;
import org.mongojack.ObjectId;

public class Profile {
	
	@Id @ObjectId
	private String _id;
	private String username;
	private String password;
	private String nom;
	private String prenom;
	private String telephone;
	private String address;
	private String email;
	private String facebookID;
	private String pictureUrl;
	
	public Profile() {
		super();
	}

	public Profile(String username,String password, String nom, String prenom, String telephone, String address, String email,String facebookID,String pictureUrl) {
		super();
		this.username 		= username;
		this.password 		= password;
		this.nom 			= nom;
		this.prenom 		= prenom;
		this.telephone 		= telephone;
		this.address 		= address;
		this.email 			= email;
		this.facebookID 	= facebookID;
		this.pictureUrl		= pictureUrl;
	}

	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	public String get_id() {
	    return _id;
	}

	public void set_id(String _id) {
	    this._id = _id;
	}

	public String getNom() {
		return nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public String getTelephone() {
		return telephone;
	}

	public String getAddress() {
		return address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFacebookID() {
		return facebookID;
	}

	public void setFacebookID(String facebookID) {
		this.facebookID = facebookID;
	}

	public String getPictureUrl() {
		return pictureUrl;
	}

	public void setPictureUrl(String pictureUrl) {
		this.pictureUrl = pictureUrl;
	}

	@Override
	public String toString() {
		return "Profile [_id=" + _id + ", username=" + username + ", password=" + password + ", nom=" + nom
				+ ", prenom=" + prenom + ", telephone=" + telephone + ", address=" + address + ", email=" + email
				+ ", facebookID=" + facebookID + ", pictureUrl=" + pictureUrl + "]";
	}
	
	

}
