package com.robekabyle.robekabylerestful.model;

/**
 * This class is the model of the photo shared by users
 * @author Acene
 *
 */
public class PostModel {

	private String model;
	private int price;
	private String description;
	
	public PostModel() {
		super();
	}
	
	public PostModel(String model, int price, String description) {
		super();
		this.model = model;
		this.price = price;
		this.description = description;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "PostModel [model=" + model + ", price=" + price + ", description=" + description + "]";
	}
	
	
}
