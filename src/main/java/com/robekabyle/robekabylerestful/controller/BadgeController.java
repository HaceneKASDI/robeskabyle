package com.robekabyle.robekabylerestful.controller;

import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.robekabyle.robekabylerestful.model.Badge;
import com.robekabyle.robekabylerestful.services.implems.ServiceBadge;
import com.robekabyle.robekabylerestful.services.implems.ServiceConnection;
import com.robekabyle.robekabylerestful.services.implems.ServiceImages;
import com.robekabyle.robekabylerestful.utils.ErrorProvider;

/**
 * This class allows Badge operations
 * we can : 
 * 		addFollower(profileID,profileTarget), 
 * 		removeFollower(profileID,profileTarget), 
 * 		getBadgeByID(profileID)
 * @author Acene 14-12-2018
 * @version 2018
 * 
 */
@RestController
public class BadgeController {

	/**
	 * This method called in HTTP POST to add new Follower
	 * @return HTTP Status 200 if all is okay 
	 * 				Status 404 if user not found and not connected
	 */
	@RequestMapping(value="/Badge/addFollower",method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE+"; charset:utf-8")
	@ResponseBody
	public ResponseEntity<String> addFollower(@RequestParam String profileID, @RequestBody String data)
	{
		try {
				
				// CHECK IF USER EXISTS AND CONNECTED
				ServiceConnection serviceConnect = new ServiceConnection();
				if(serviceConnect.isUserConnected(profileID)) {
				/** data modal {"badgeID": "", "followerID" : ""} */
				JSONObject json = new JSONObject(data);
					
				ServiceBadge serviceBadge = new ServiceBadge();
				String msgFollower = serviceBadge.saveFollower(json.optString("badgeID"), json.optString("followerID"));
				
				serviceBadge.closeConnection();
				serviceConnect.closeConnection();
				return new ResponseEntity<String>(msgFollower, HttpStatus.OK);
				}else {
					return new ResponseEntity<String>(ErrorProvider.USER_DISCONNECTED, HttpStatus.NOT_FOUND);
				}
			
		} catch (Exception e) {
			return new ResponseEntity<String>(ErrorProvider.BAD_MANIPULATION, HttpStatus.REQUESTED_RANGE_NOT_SATISFIABLE);
		}
	}
	
	
	@RequestMapping(value="/Badge/deleteFollower",method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE+"; charset:utf-8")
	@ResponseBody
	public ResponseEntity<String> deleteFollower(@RequestParam String profileID, @RequestBody String data)
	{
		try {
			
			// CHECK IF USER EXISTS AND CONNECTED
			ServiceConnection serviceConnect = new ServiceConnection();
			if(serviceConnect.isUserConnected(profileID)) {
			/** data modal {"badgeID": "", "followerID" : ""} */
			JSONObject json = new JSONObject(data);
				
			ServiceBadge serviceBadge = new ServiceBadge();
			String msgFollower = serviceBadge.deleteFollower(json.optString("badgeID"), json.optString("followerID"));
			
			serviceBadge.closeConnection();
			serviceConnect.closeConnection();
			return new ResponseEntity<String>(msgFollower, HttpStatus.OK);
			}else {
				return new ResponseEntity<String>(ErrorProvider.USER_DISCONNECTED, HttpStatus.NOT_FOUND);
			}
		
	} catch (Exception e) {
		return new ResponseEntity<String>(ErrorProvider.BAD_MANIPULATION, HttpStatus.REQUESTED_RANGE_NOT_SATISFIABLE);
	}
	}
	
	
	@RequestMapping(value="/Badge/updateBadge",method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE+"; charset:utf-8")
	@ResponseBody
	public ResponseEntity<String> updateBadge(@RequestBody Badge badge)
	{
		try {
			
			// CHECK IF USER EXISTS AND CONNECTED
			ServiceConnection serviceConnect = new ServiceConnection();
			if(serviceConnect.isUserConnected(badge.getIdProfile())) {
				
			ServiceBadge serviceBadge = new ServiceBadge();
			String msgUpdating = serviceBadge.updateBadge(badge);
			
			serviceBadge.closeConnection();
			serviceConnect.closeConnection();
			return new ResponseEntity<String>(msgUpdating , HttpStatus.OK);
			}else {
				return new ResponseEntity<String>(ErrorProvider.USER_DISCONNECTED, HttpStatus.NOT_FOUND);
			}
		
	} catch (Exception e) {
		return new ResponseEntity<String>(ErrorProvider.BAD_MANIPULATION, HttpStatus.REQUESTED_RANGE_NOT_SATISFIABLE);
	}
	}
	
	/**
	 * 	
	 * @param profileID
	 * @return
	 */
	@RequestMapping(value="/Badge/getBadge",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE+"; charset:utf-8")
	@ResponseBody
	public ResponseEntity<Badge> getBadgeByProfile(@RequestParam String profileID)
	{
		try {
				ServiceBadge serviceBadge = new ServiceBadge();
				Badge badgeUser = serviceBadge.getProfileByUserID(profileID);
				
				ServiceImages serviceImages = new ServiceImages();
				int nbrPosts = serviceImages.getImagesByUserID(profileID).size();
				badgeUser.setNbrPosts(nbrPosts);
				
				serviceBadge.closeConnection();
				return new ResponseEntity<Badge>(badgeUser, HttpStatus.OK);

	} catch (Exception e) {
		return new ResponseEntity<Badge>(new Badge(), HttpStatus.REQUESTED_RANGE_NOT_SATISFIABLE);
	}
	}
	
}
