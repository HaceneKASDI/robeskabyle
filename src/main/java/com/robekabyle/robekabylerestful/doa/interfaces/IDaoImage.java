package com.robekabyle.robekabylerestful.doa.interfaces;

import java.util.List;

import com.robekabyle.robekabylerestful.model.Filter;
import com.robekabyle.robekabylerestful.model.ImagePost;

public interface IDaoImage extends IDaoGeneric<ImagePost> {
	
	public List<ImagePost> searchModal(String modalDress);
	public List<ImagePost> searchByToken(String searchToken);
	public List<ImagePost> searchByFilter(Filter filter);
	public List<ImagePost> searchByUserID(String profileID);
	public void closeConnection();
}
