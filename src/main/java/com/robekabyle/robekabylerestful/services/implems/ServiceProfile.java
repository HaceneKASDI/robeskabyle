package com.robekabyle.robekabylerestful.services.implems;

import java.util.List;

import com.robekabyle.robekabylerestful.doa.implems.DaoProfile;
import com.robekabyle.robekabylerestful.model.Profile;
import com.robekabyle.robekabylerestful.services.interfaces.IServiceProfile;

/**
 * 
 * @author Hacene KASDI
 * 11/2018
 * 
 * This class allows access to DAOProfile to add, update, delete user profile
 * this service can be invoked when we try to login with facebook using the 
 * uri : /register/withfacebook or 
 * directly from the register form of front End using the uri : /register
 */
public class ServiceProfile implements IServiceProfile {

	DaoProfile daoProfile = new DaoProfile();
	
	/**
	 * This function returns all Profiles stored in database
	 */
	public List<Profile> getAllprofiles() {
		return daoProfile.getAll();
	}

	/**
	 * This function create a new profile returns the_id of the saved Profile
	 */
	public String saveNewProfile(Profile profile) {
		return daoProfile.save(profile);
	}

	/**
	 * This function update a given Profile
	 */
	public String updateProfile(Profile profile) {
		return daoProfile.update(profile);
	}

	/**
	 * This function gets the profile according to a given _id of record Profile 
	 */
	public Profile getProfileByID(String userID) {
		return daoProfile.get(userID);
	}

	/**
	 * This function removes the profile by _id of the current record related to Profile
	 */
	public String deleteProfile(String userID) {
		return daoProfile.delete(userID);
	}

	/**
	 * This function returns a user according to the userID
	 */
	public Profile getProfileByUserID(String userID) {
		return daoProfile.getProfileByUSERID(userID);
	}

	/**
	 * Get Profile according to username
	 * This function used to check if there is no double entry
	 */
	public Profile getProfileByUsername(String username) {
		return daoProfile.getProfileByUsername(username);
	}
	
	public void closeConnection() {
		daoProfile.closeConnection();
	}

}
