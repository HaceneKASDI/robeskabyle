package com.robekabyle.robekabylerestful.services.interfaces;

import java.util.List;

import com.robekabyle.robekabylerestful.model.ConnectionToken;

public interface IServiceConnection {
	
	public List<ConnectionToken> 	getAllConnections();
	public String 					saveNewConnection(ConnectionToken entity);
	public String 					updateConnection(ConnectionToken entity);
	public ConnectionToken 			getConnectionByUsername(String username);
	public ConnectionToken 			authenticateUser(String username, String password);
	public ConnectionToken 			getConnectionByFacebookID(String facebookID);
	public String 					logoutUser(ConnectionToken entity);
	public boolean					isUserConnected(String userProfile);
	public void						closeConnection();

	
}
