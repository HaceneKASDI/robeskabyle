package com.robekabyle.robekabylerestful.doa.implems;

import java.util.List;

import org.bson.types.ObjectId;
import org.mongojack.JacksonDBCollection;
import org.mongojack.WriteResult;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.robekabyle.robekabylerestful.doa.DaoGeneric;
import com.robekabyle.robekabylerestful.doa.interfaces.IDaoProfile;
import com.robekabyle.robekabylerestful.model.Profile;
import com.robekabyle.robekabylerestful.utils.ErrorProvider;
/**
 * 
 * @author Hacene KASDI
 * 11/2018
 * This class is a Data access layer used to CRUD application users
 */
public class DaoProfile extends 	DaoGeneric<Profile> 
						implements 	IDaoProfile {
	
	JacksonDBCollection<Profile, String> collection;

	public DaoProfile() {
		super();
		this.collection = super.getCollection("profile_rk", Profile.class);
	}
	
	public List<Profile> getAll() {
		 return this.collection.find().toArray();
	}

	public String save(Profile entity) {
		WriteResult<Profile, String> result = this.collection.insert(entity);
		return	result.getDbObject().get("_id").toString();
	}

	public void closeConnection() {
		super.closeConnection();
	}
	
	public String update(Profile entity) {
		try {
			collection.updateById(entity.get_id(), entity);
			return ErrorProvider.OK_MONGODB;
		}catch (Exception e) {
			return ErrorProvider.ERROR_UPDATING_MONGODB;
		}
	}

	public Profile get(String id) {
		Profile profile =	collection.findOneById(id);
		super.closeConnection();
		return profile;
	}

	public String delete(String id) {
		try {
			collection.removeById(id);
			return ErrorProvider.OK_MONGODB;
		}catch (Exception e) {
			return ErrorProvider.ERROR_DELETING_MONGODB;
		}
	}

	public Profile getProfileByUSERID(String userID) {
		try {
			ObjectId id= new ObjectId(userID);   
			DBObject query = new BasicDBObject("_id", id);
			return collection.findOne(query);
		} catch (Exception e) {
			return null;
		}
	}

	public Profile getProfileByUsername(String username) {
		try {
			DBObject query = new BasicDBObject("username", username);
			return collection.findOne(query);
		} catch (Exception e) {
			return null;
		}
	}

}
