package com.robekabyle.robekabylerestful.model;

import org.mongojack.Id;
import org.mongojack.ObjectId;

public class ConnectionToken {
	
	@Id @ObjectId
	private String _id;
	private String userId;
	private String username;
	private String password;
	private String lastConnection;
	private boolean isConnected;
	private String facebookID;
	private String observation;
	
	public ConnectionToken() {
		super();
	}

	public ConnectionToken(String userId, String username, String password, 
			String lastConnection, boolean isConnected, String facebookID, String observation) {
		super();
		this.userId = userId;
		this.username = username;
		this.password = password;
		this.lastConnection = lastConnection;
		this.isConnected = isConnected;
		this.facebookID = facebookID;
		this.observation = observation;
		
	}

	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	public boolean getIsConnected() {
		return isConnected;
	}

	public void setIsConnected(boolean isConnected) {
		this.isConnected = isConnected;
	}

	public String getFacebookID() {
		return facebookID;
	}

	public void setFacebookID(String facebookID) {
		this.facebookID = facebookID;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getLastConnection() {
		return lastConnection;
	}

	public void setLastConnection(String lastConnection) {
		this.lastConnection = lastConnection;
	}

	public String getObservation() {
		return observation;
	}

	public void setObservation(String observation) {
		this.observation = observation;
	}

	public void setConnected(boolean isConnected) {
		this.isConnected = isConnected;
	}

	@Override
	public String toString() {
		return "ConnectionToken [_id=" + _id + ", userId=" + userId + ", username=" + username + ", password="
				+ password + ", lastConnection=" + lastConnection + ", isConnected=" + isConnected + ", facebookID="
				+ facebookID + ", observation=" + observation + "]";
	}
	
	

}
