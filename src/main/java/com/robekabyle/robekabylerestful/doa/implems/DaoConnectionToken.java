package com.robekabyle.robekabylerestful.doa.implems;

import java.util.List;

import org.mongojack.JacksonDBCollection;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.robekabyle.robekabylerestful.doa.DaoGeneric;
import com.robekabyle.robekabylerestful.doa.interfaces.IDaoConnectionToken;
import com.robekabyle.robekabylerestful.model.ConnectionToken;
import com.robekabyle.robekabylerestful.utils.ErrorProvider;

/**
 * 
 * @author Hacene KASDI
 * 11/2018
 * 
 * This class is a data access layer used to authenticate users
 */
public class DaoConnectionToken 	extends DaoGeneric<ConnectionToken> 
									implements IDaoConnectionToken {
	
	JacksonDBCollection<ConnectionToken, String> collection;
	
	public DaoConnectionToken() {
		super();
		this.collection = super.getCollection("connection_rk", ConnectionToken.class);
	}

	public List<ConnectionToken> getAll() {
		return collection.find().toArray();
	}

	public String save(ConnectionToken entity) {
		return collection.save(entity).getSavedId();
	}

	public String update(ConnectionToken entity) {
		try {
		collection.updateById(entity.get_id(), entity);
		return ErrorProvider.OK_MONGODB;
		}catch (Exception e) {
			return ErrorProvider.ERROR_UPDATING_MONGODB;
		}
	}

	public ConnectionToken get(String id) {
		return collection.findOneById(id);
	}

	public String delete(String id) {
		try {
			collection.removeById(id);
			return ErrorProvider.OK_MONGODB;
		}catch (Exception e) {
			return ErrorProvider.ERROR_DELETING_MONGODB;
		}
	}

	public ConnectionToken findConnectionByUsername(String username) {
		try {
			 DBObject query = new BasicDBObject("username", username);
			 return collection.findOne(query);
		} catch (Exception e) {
			return null;
		}
	}

	public ConnectionToken findConnectionByFacebookID(String facebookID) {
		try {
			 DBObject query = new BasicDBObject("facebookID", facebookID);
			 return collection.findOne(query);
		} catch (Exception e) {
			return null;
		}
	}

	public ConnectionToken findAutheticatedUser(String username, String password) {
		try {
			 DBObject query = new BasicDBObject("username", username).append("password", password);
			 return collection.findOne(query);
		} catch (Exception e) {
			super.closeConnection();
			return null;
		}
	}

	@Override
	public boolean IsAuthenticated(String profileID) {
		DBObject query = new BasicDBObject("userId", profileID).append("isConnected", true);
		int nbrConnectionsFound = this.collection.find(query).toArray().size();
		System.out.println("#### >> IS USER CONNECTED = "+nbrConnectionsFound);
		if(nbrConnectionsFound > 0 ) return true; else return false;
	}
	
	public void closeConnection() {
		super.closeConnection();
	}
}
