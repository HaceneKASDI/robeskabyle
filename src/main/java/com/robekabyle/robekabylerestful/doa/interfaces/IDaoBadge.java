package com.robekabyle.robekabylerestful.doa.interfaces;

import com.robekabyle.robekabylerestful.model.Badge;

public interface IDaoBadge extends IDaoGeneric<Badge>{
	
	public Badge getBadgeByProfile(String profileId);

}
