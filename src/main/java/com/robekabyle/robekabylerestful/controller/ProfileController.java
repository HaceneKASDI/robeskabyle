package com.robekabyle.robekabylerestful.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.robekabyle.robekabylerestful.model.ConnectionToken;
import com.robekabyle.robekabylerestful.model.ImagePost;
import com.robekabyle.robekabylerestful.model.Profile;
import com.robekabyle.robekabylerestful.services.implems.ServiceConnection;
import com.robekabyle.robekabylerestful.services.implems.ServiceImages;
import com.robekabyle.robekabylerestful.services.implems.ServiceProfile;
import com.robekabyle.robekabylerestful.utils.ErrorProvider;

/**
 * 
 * @author Acene 11-12-2018
 * @version 2018
 * 
 * This class defines the actions done for Profile user
 * we can read a profile according to the <code>userID</code>
 * we van update profile according to <code>Profile</code>
 * we can delete profile according to <code>userID</code> 
 */
@RestController
public class ProfileController {

	/**
	 * This method LOGIN the use it seeks the user <code>Profile</code> 
	 * related to <code>ConnectionToken</code>
	 * 
	 * @param userConnection : the token of the connection
	 * @param userID : profile ID
	 * @return The request JSON presentation of <code>Profile</code>
	 */
	@RequestMapping(value="/profile/{userID}",method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE+"; charset:utf-8")
	@ResponseBody
	public ResponseEntity<Profile> loginUser(@RequestBody ConnectionToken userConnection, @PathVariable String userID)
	{
		ServiceProfile serviceProfile = new ServiceProfile();
		Profile profile = serviceProfile.getProfileByUserID(userID);
		serviceProfile.closeConnection();
		if(profile != null) return new ResponseEntity<Profile>(profile, HttpStatus.OK);
		else return new ResponseEntity<Profile>(profile, HttpStatus.NOT_FOUND);
	}
	
	/**
	 * This method UPDATE the user <code>Profile</code>
	 * @param profile : the <code>Profile</code> to update
	 * @return : Message success if the updating was well done, error message if failure
	 */
	@RequestMapping(value="/updateProfile",method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE+"; charset:utf-8")
	@ResponseBody
	public ResponseEntity<String> updateUser(@RequestBody Profile profile)
	{
		ServiceProfile serviceProfile = new ServiceProfile();
		Profile ifExistProfile = serviceProfile.getProfileByUserID(profile.get_id());
		
		if(ifExistProfile != null) {
			String responseBD = serviceProfile.updateProfile(profile);
			if(ErrorProvider.OK_MONGODB == responseBD) {
				// We have to update login token of connection
				ServiceConnection connectionService = new ServiceConnection();
				ConnectionToken tokenConnection = connectionService.getConnectionByUsername(profile.getUsername());
				tokenConnection.setPassword(profile.getPassword());
				connectionService.updateConnection(tokenConnection);
				
				// We have to update post images fullName
				ServiceImages serviceImages = new ServiceImages();
				List<ImagePost> imagesList = serviceImages.getImagesByUserID(profile.get_id());
				imagesList.stream().forEach(post -> post.setFullName(profile.getPrenom()+" "+profile.getNom()));
				imagesList.stream().forEach(element -> serviceImages.updateImage(element));
				
				// CLOSE CONNECTIONS
				connectionService.closeConnection();
				serviceProfile.closeConnection();
				serviceImages.closeConnection();
			return new ResponseEntity<String>("Utilisateur mis à jour !", HttpStatus.OK);
			
			}else { 
					// CLOSE CONNECTIONS
					serviceProfile.closeConnection();
					return new ResponseEntity<String>("Une erreur est survenue !", HttpStatus.INTERNAL_SERVER_ERROR);
				  }
		} else {
			// CLOSE CONNECTIONS
			serviceProfile.closeConnection();
			return new ResponseEntity<String>("Cet utilisateur n'existe pas !", HttpStatus.NOT_FOUND);
		}
	}
	
	/**
	 * This method DELETE the <code>Profile</code>
	 * @param IDprofile : the id of the <code>Profile</code> to delete
	 * @return : Message success if the updating was well done, error message if failure
	 */
	@RequestMapping(value="/deleteProfile",method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE+"; charset:utf-8")
	@ResponseBody
	public ResponseEntity<String> deleteUser(@RequestBody String IDprofile)
	{
		ServiceProfile serviceProfile = new ServiceProfile();
		Profile ifExistProfile = serviceProfile.getProfileByUserID(IDprofile);
		if(ifExistProfile != null) {
			String responseBD = serviceProfile.deleteProfile(IDprofile);
			// CLOSE CONNECTIONS
			serviceProfile.closeConnection();
			if(ErrorProvider.OK_MONGODB == responseBD)
			return new ResponseEntity<String>("Utilisateur supprimé !", HttpStatus.OK);
				else return new ResponseEntity<String>("Une erreur est survenue !", HttpStatus.INTERNAL_SERVER_ERROR);
		} else return new ResponseEntity<String>("Cet utilisateur n'existe pas !", HttpStatus.NOT_FOUND);
	}
}
