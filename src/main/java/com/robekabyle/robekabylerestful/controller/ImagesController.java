package com.robekabyle.robekabylerestful.controller;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.robekabyle.robekabylerestful.model.ImagePost;
import com.robekabyle.robekabylerestful.model.Profile;
import com.robekabyle.robekabylerestful.services.implems.ServiceConnection;
import com.robekabyle.robekabylerestful.services.implems.ServiceImages;
import com.robekabyle.robekabylerestful.services.implems.ServiceProfile;
import com.robekabyle.robekabylerestful.utils.ErrorProvider;
import com.robekabyle.robekabylerestful.utils.UtilsFactory;

/**
 * 
 * @author Acene 18-12-2018
 * @version 2018
 * 
 * This class defines the actions done for Posted images user 
 */
@RestController
public class ImagesController {

	@RequestMapping(value="/PostImage/getPostImages",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE+"; charset:utf-8")
	@ResponseBody
	public ResponseEntity<List<ImagePost>> getPostImage(@RequestParam String userId)
	{
		try {
			// CHECK IF USER EXISTS AND CONNECTED
			ServiceConnection serviceConnect = new ServiceConnection();
			if(serviceConnect.isUserConnected(userId)) {
			ServiceImages serviceImages = new ServiceImages();
			List<ImagePost> listImages = serviceImages.getAllImages();
			
			// CLOSE CONNECTIONS
			serviceConnect.closeConnection();
			serviceImages.closeConnection();
			return new ResponseEntity<List<ImagePost>>(listImages, HttpStatus.OK);
			}else {
				return new ResponseEntity<List<ImagePost>>(new ArrayList<ImagePost>(), HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<List<ImagePost>>(new ArrayList<ImagePost>(), HttpStatus.REQUESTED_RANGE_NOT_SATISFIABLE);
		}
	}
	
	
	@RequestMapping(value="/PostImage/getPostsByUser",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE+"; charset:utf-8")
	@ResponseBody
	public ResponseEntity<List<ImagePost>> getPostsByUser(@RequestParam String userId)
	{
		try {
			ServiceImages serviceImages = new ServiceImages();
			List<ImagePost> listImages = serviceImages.getImagesByUserID(userId);
			
			// CLOSE CONNECTIONS
			serviceImages.closeConnection();
			return new ResponseEntity<List<ImagePost>>(listImages, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<List<ImagePost>>(new ArrayList<ImagePost>(), HttpStatus.REQUESTED_RANGE_NOT_SATISFIABLE);
		}
	}
	
	
	/**
	 * 
	 * @param image : PostImage sent by the user to save in the database
	 * @param userID
	 * @return
	 */
	@RequestMapping(value="/PostImage/addPostImage",method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE+"; charset:utf-8")
	@ResponseBody
	public ResponseEntity<String> savePostImage(@RequestBody ImagePost image)
	{
		try {
			// CHECK IF USER EXISTS AND CONNECTED
			ServiceConnection serviceConnect = new ServiceConnection();
			if(serviceConnect.isUserConnected(image.getIdProfile())) {
			ServiceProfile serviceProfile = new ServiceProfile();
			Profile userProfile = serviceProfile.getProfileByUserID(image.getIdProfile());
			
			ImagePost imageToStore = 
					new ImagePost(image.getIdProfile(), image.getUrlImagePost(), image.getTextPost(), userProfile.getPrenom()+" "+userProfile.getNom(), 
					userProfile.getPictureUrl(), UtilsFactory.getCurrentDate(), image.getModelDress());
			
			ServiceImages serviceImages = new ServiceImages();
			serviceImages.saveNewImage(imageToStore);
			
			// CLOSE CONNECTIONS
			serviceConnect.closeConnection();
			serviceImages.closeConnection();
			serviceImages.closeConnection();
			return new ResponseEntity<String>(ErrorProvider.POST_IMAGE_OK, HttpStatus.OK);
			}else {
				return new ResponseEntity<String>(ErrorProvider.USER_DISCONNECTED, HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<String>(ErrorProvider.BAD_MANIPULATION, HttpStatus.REQUESTED_RANGE_NOT_SATISFIABLE);
		}
	}
	
	@RequestMapping(value="/PostImage/updatePostImage",method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE+"; charset:utf-8")
	@ResponseBody
	public ResponseEntity<String> updatePostImage(@RequestBody ImagePost image)
	{
		try {

			// CHECK IF USER EXISTS AND CONNECTED
			ServiceConnection serviceConnect = new ServiceConnection();
			if(serviceConnect.isUserConnected(image.getIdProfile())) {
			ServiceImages serviceImages = new ServiceImages();
			String msg = serviceImages.updateImage(image);
			
			// CLOSE CONNECTIONS
			serviceConnect.closeConnection();
			serviceImages.closeConnection();
			
			if(msg.equals(ErrorProvider.OK_MONGODB))
				return new ResponseEntity<String>(ErrorProvider.OK_MONGODB, HttpStatus.OK);
			else
				return new ResponseEntity<String>(ErrorProvider.ERROR_UPDATING_MONGODB, HttpStatus.INTERNAL_SERVER_ERROR);
			}else {
				return new ResponseEntity<String>(ErrorProvider.USER_DISCONNECTED, HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			System.out.println("#### UPDATE IMAGE POST : "+e);
			return new ResponseEntity<String>(ErrorProvider.BAD_MANIPULATION, HttpStatus.REQUESTED_RANGE_NOT_SATISFIABLE);
		}
	}
	
	/**
	 * The image must be deleted in the path of the server
	 * @param profileID
	 * @param imageID
	 * @return
	 */
	@RequestMapping(value="/PostImage/deletePostImage",method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE+"; charset:utf-8")
	@ResponseBody
	public ResponseEntity<String> deletePostImage(@RequestParam String profileID, @RequestBody String imageID)
	{
		
		try {
			System.out.println("PROFILE ID = "+profileID+"  |  IMAGE ID = "+imageID);
			// CHECK IF USER EXISTS AND CONNECTED
			ServiceConnection serviceConnect = new ServiceConnection();
			if(serviceConnect.isUserConnected(profileID)) {
				JSONObject json = new JSONObject(imageID);
				System.out.println(json.get("imageID"));
				
			ServiceImages serviceImages = new ServiceImages();
			
			String msg = serviceImages.deleteImage(json.get("imageID").toString());
			System.err.println("===> "+msg);
			
			// CLOSE CONNECTIONS
			serviceConnect.closeConnection();
			serviceImages.closeConnection();
			
			if(msg.equals(ErrorProvider.OK_MONGODB))
				return new ResponseEntity<String>(ErrorProvider.OK_MONGODB, HttpStatus.OK);
			else
				return new ResponseEntity<String>(ErrorProvider.ERROR_UPDATING_MONGODB, HttpStatus.INTERNAL_SERVER_ERROR);
			}else {
				return new ResponseEntity<String>(ErrorProvider.USER_DISCONNECTED, HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			System.out.println("#### UPDATE IMAGE POST : "+e);
			return new ResponseEntity<String>(ErrorProvider.BAD_MANIPULATION, HttpStatus.REQUESTED_RANGE_NOT_SATISFIABLE);
		}
	}
	
}
