package com.robekabyle.robekabylerestful.tests;

import com.robekabyle.robekabylerestful.doa.implems.DaoConnectionToken;
import com.robekabyle.robekabylerestful.model.ConnectionToken;
import com.robekabyle.robekabylerestful.services.implems.ServiceConnection;
import com.robekabyle.robekabylerestful.utils.UtilsFactory;

public class DAOTester {

	public static void main(String[] args) {
		
		// Insert new Entity in the database
		DaoConnectionToken daoConnection = new DaoConnectionToken(); 
		
		// ===================================================================
		//								CREATE
		// ===================================================================
		//ConnectionToken conn = new ConnectionToken("OOOOOOOO7777", "FORZA", "FOOOT", UtilsFactory.getCurrentDate(),true,"HHGGFF");
		//String idEntity = daoConnection.save(conn);
		//System.out.println(idEntity);
		
		// get The added object using the _id
		//ConnectionToken connection = daoConnection.get(idEntity);
		//System.out.println("Last connection : "+connection.getLastConnection());
		
		// ===================================================================
		//								UPDATE
		// ===================================================================
		// Update the entity
		//String newDate = UtilsFactory.getCurrentDate();
		//connection.setLastConnection(newDate);
		//String result = daoConnection.update(connection);
		//System.out.println("Is updated : "+result);
		
		// after updating
		//ConnectionToken conne2 = daoConnection.get(connection.get_id());
		//System.out.println("Last connection : "+conne2.getLastConnection());
		
		
		// ===================================================================
		//								DELETE
		// ===================================================================
		//String resDelete = daoConnection.delete("5be604f9597d962ba8de513f");
		//System.out.println("Is deleted : "+resDelete);
		
		// ===================================================================
		//								GET USER BY username or facebookID
		// ===================================================================
		
		ConnectionToken user = daoConnection.findConnectionByUsername("FORZA");
		System.out.println(user.getLastConnection()+"  user is connected : "+user.getIsConnected());
		
		ServiceConnection connectionService = new ServiceConnection();
		connectionService.logoutUser(user);
		
		ConnectionToken user2 = daoConnection.findConnectionByUsername("FORZA");
		System.out.println(user2.getLastConnection()+"  user is connected : "+user2.getIsConnected());
		
		ConnectionToken userIsAuthenticated = daoConnection.findAutheticatedUser("FORZA", "FOOOT");
		System.out.println(userIsAuthenticated.getLastConnection()+"  user is authenticated : "+userIsAuthenticated.getIsConnected());
		
		
	}

}
