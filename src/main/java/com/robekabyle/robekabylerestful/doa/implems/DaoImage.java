package com.robekabyle.robekabylerestful.doa.implems;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

import org.mongojack.JacksonDBCollection;
import org.mongojack.WriteResult;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.robekabyle.robekabylerestful.doa.DaoGeneric;
import com.robekabyle.robekabylerestful.doa.interfaces.IDaoImage;
import com.robekabyle.robekabylerestful.model.Filter;
import com.robekabyle.robekabylerestful.model.ImagePost;
import com.robekabyle.robekabylerestful.utils.ErrorProvider;

public class DaoImage 	extends 	DaoGeneric<ImagePost> 
						implements 	IDaoImage {
	
	JacksonDBCollection<ImagePost, String> collection;
	
	public DaoImage() {
		super();
		this.collection = super.getCollection("images_rk", ImagePost.class);
	}
	
	public List<ImagePost> getAll() {
		DBObject query = new BasicDBObject("dateShare", 1);
		List<ImagePost> images = this.collection.find().sort(query).toArray();
		Collections.reverse(images);
		return images;
	}

	public String save(ImagePost entity) {
		WriteResult<ImagePost, String> result = this.collection.insert(entity);
		return result.getDbObject().get("_id").toString();
		
	}

	public String update(ImagePost entity) {
		try {
			this.collection.updateById(entity.get_id(), entity);
			return ErrorProvider.OK_MONGODB;
		} catch (Exception e) {
			return ErrorProvider.ERROR_UPDATING_MONGODB;
		}
		
	}

	public ImagePost get(String id) {
		return this.collection.findOneById(id);
	}

	public String delete(String id) {
		try {
			collection.removeById(id);
			return ErrorProvider.OK_MONGODB;
		}catch (Exception e) {
			return ErrorProvider.ERROR_DELETING_MONGODB;
		}
	}

	@Override
	public List<ImagePost> searchModal(String modalDress) {
		List<ImagePost> images = new ArrayList<ImagePost>();
		try {
			
			BasicDBObject query = new 
			BasicDBObject("modelDress.model", Pattern.compile(modalDress+".*" , Pattern.CASE_INSENSITIVE));
			return this.collection.find(query).toArray();
			
		} catch (Exception e) {
			System.out.println(e);
			System.out.println(ErrorProvider.ERROR_SEARCHING_MONGODB);
			return images;
		}
	}
	
	@Override
	public List<ImagePost> searchByFilter(Filter filter) {
		List<ImagePost> images = new ArrayList<ImagePost>();
		try {
			DBObject clause1 = new BasicDBObject("modelDress.model", Pattern.compile(filter.getModal()+".*" , Pattern.CASE_INSENSITIVE));    
			DBObject clause2 = new BasicDBObject("modelDress.price", new BasicDBObject("$lte", filter.getPrice()));
			System.out.println("================");
			System.out.println(clause1.toString());
			System.out.println(clause2.toString());
			BasicDBList and = new BasicDBList();
			and.add(clause1);
			and.add(clause2);

			DBObject query = new BasicDBObject("$and", and);
			List<ImagePost> imagesSorted = this.collection.find(query).toArray();
			Collections.reverse( imagesSorted);
			return imagesSorted ;
			
		} catch (Exception e) {
			System.out.println(e);
			System.out.println(ErrorProvider.ERROR_SEARCHING_MONGODB);
			return images;
		}
	}

	@Override
	public List<ImagePost> searchByToken(String searchToken) {
		List<ImagePost> images = new ArrayList<ImagePost>();
		try {
			DBObject clause1 = new BasicDBObject("textPost", Pattern.compile(searchToken+".*" , Pattern.CASE_INSENSITIVE));  
			DBObject clause2 = new BasicDBObject("modelDress.model", Pattern.compile(searchToken+".*" , Pattern.CASE_INSENSITIVE));    
			DBObject clause3 = new BasicDBObject("modelDress.description", Pattern.compile(searchToken+".*" , Pattern.CASE_INSENSITIVE));    
			BasicDBList or = new BasicDBList();
			or.add(clause1);
			or.add(clause2);
			or.add(clause3);
			

			DBObject query = new BasicDBObject("$or", or);
			List<ImagePost> imagesSorted = this.collection.find(query).toArray();
			Collections.reverse( imagesSorted);
			return imagesSorted ;
			
		} catch (Exception e) {
			System.out.println(e);
			System.out.println(ErrorProvider.ERROR_SEARCHING_MONGODB);
			return images;
		}
	}

	@Override
	public List<ImagePost> searchByUserID(String profileID) {
		List<ImagePost> images = new ArrayList<ImagePost>();
		try {
			
			BasicDBObject query = new 
					BasicDBObject("idProfile", profileID);
			return this.collection.find(query).toArray();
			
		} catch (Exception e) {
			System.out.println(e);
			System.out.println(ErrorProvider.ERROR_SEARCHING_MONGODB);
			return images;
		}
	}
	
	public void closeConnection() {
		super.closeConnection();
	}


}
