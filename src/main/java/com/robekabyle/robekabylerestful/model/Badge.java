package com.robekabyle.robekabylerestful.model;

import java.util.ArrayList;
import java.util.List;

import org.mongojack.Id;
import org.mongojack.ObjectId;

public class Badge {
	
	@Id @ObjectId
	private String 	_id;
	private String 	idProfile;
	private int	   	userBadge; // 1 : Beginner | 2 : Professional | 3 : Expert 
	private String 	biographie;
	private String 	facebookPage;
	private String 	instagramPage;
	private String  addressLocal;
	private int 	nbrPosts;
	private List<String> likes;
	private List<String> followers;
	private List<Filter> filters;
	

	public Badge() {
		super();
	}

	public Badge(String idProfile, int userBadge, String biographie) {
		super();
		this.idProfile 	= idProfile;
		this.userBadge 	= userBadge;
		this.biographie = biographie;
	}

	public Badge(String idProfile, int userBadge, String biographie, String facebookPage, String instagramPage, String addressLocal) {
		super();
		this.idProfile 		= idProfile;
		this.userBadge 		= userBadge;
		this.biographie 	= biographie;
		this.facebookPage 	= facebookPage;
		this.instagramPage 	= instagramPage;
		this.addressLocal	= addressLocal;
		this.likes 			= new ArrayList<String>();
		this.followers 		= new ArrayList<String>();
		this.filters		= new ArrayList<Filter>();
	}

	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	public String getIdProfile() {
		return idProfile;
	}

	public void setIdProfile(String idProfile) {
		this.idProfile = idProfile;
	}

	public int getUserBadge() {
		return userBadge;
	}

	public void setUserBadge(int userBadge) {
		this.userBadge = userBadge;
	}

	public String getBiographie() {
		return biographie;
	}

	public void setBiographie(String biographie) {
		this.biographie = biographie;
	}

	public String getFacebookPage() {
		return facebookPage;
	}

	public void setFacebookPage(String facebookPage) {
		this.facebookPage = facebookPage;
	}

	public String getInstagramPage() {
		return instagramPage;
	}

	public void setInstagramPage(String instagramPage) {
		this.instagramPage = instagramPage;
	}
	
	public int getNbrPosts() {
		return nbrPosts;
	}

	public void setNbrPosts(int nbrPosts) {
		this.nbrPosts = nbrPosts;
	}

	public List<String> getLikes() {
		return likes;
	}

	public void setLikes(List<String> likes) {
		this.likes = likes;
	}

	public List<String> getFollowers() {
		return followers;
	}

	public void setFollowers(List<String> followers) {
		this.followers = followers;
	}

	
	public String getAddressLocal() {
		return addressLocal;
	}

	public void setAddressLocal(String addressLocal) {
		this.addressLocal = addressLocal;
	}
	
	public List<Filter> getFilters() {
		return filters;
	}

	public void setFilters(List<Filter> filters) {
		this.filters = filters;
	}

	@Override
	public String toString() {
		return "Badge [_id=" + _id + ", idProfile=" + idProfile + ", userBadge=" + userBadge + ", biographie="
				+ biographie + ", facebookPage=" + facebookPage + ", instagramPage=" + instagramPage + ", addressLocal="
				+ addressLocal + ", nbrPosts=" + nbrPosts + ", likes=" + likes + ", followers=" + followers
				+ ", filters=" + filters + "]";
	}

	

	

	
	
}
