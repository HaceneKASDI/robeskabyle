package com.robekabyle.robekabylerestful.services.interfaces;

import java.util.List;

import com.robekabyle.robekabylerestful.model.Badge;

public interface IServiceBadge {
	public List<Badge> 				getAllBadges();
	public String 					saveNewBadge(Badge badge);
	public String 					saveFollower(String badgeID, String follower);
	public String 					deleteFollower(String badgeID, String follower);
	public String 					updateBadge(Badge badge);
	public Badge		 			getBadgeByID(String id);
	public Badge		 			getProfileByUserID(String userID);
	public String					deleteBadge(String badgeID);
	public void						closeConnection();

}
