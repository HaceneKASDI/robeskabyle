package com.robekabyle.robekabylerestful.controller;

import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.robekabyle.robekabylerestful.model.Filter;
import com.robekabyle.robekabylerestful.services.implems.ServiceFilter;
import com.robekabyle.robekabylerestful.utils.ErrorProvider;

@RestController
public class FiltersController {

	@RequestMapping(value="/Filters/addFilter",method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE+"; charset:utf-8")
	@ResponseBody
	public ResponseEntity<String> addFilter(@RequestBody String data)
	{
		try {
				
				/** data modal {"badgeId": "", "modalName" : "" , "modal" : "" , "price" : ""} */
				JSONObject json = new JSONObject(data);
					
				ServiceFilter filterService = new ServiceFilter();
				Filter filter = new Filter(json.optString("modalName"), json.optString("modal"), json.optInt("price"));
				filterService.saveNewFilter(json.optString("badgeId"), filter);
				filterService.closeConnection();
				return new ResponseEntity<String>("Filtre ajouté !", HttpStatus.OK);
			
		} catch (Exception e) {
			System.out.println("ERROR ADDING FILTER : "+e);
			return new ResponseEntity<String>(ErrorProvider.BAD_MANIPULATION, HttpStatus.REQUESTED_RANGE_NOT_SATISFIABLE);
		}
	}
	
	
	@RequestMapping(value="/Filters/deleteFilter",method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE+"; charset:utf-8")
	@ResponseBody
	public ResponseEntity<String> deleteFilter(@RequestBody String data)
	{
		try {
		/** data {badgeId, idFilter} */
		JSONObject json = new JSONObject(data);
		ServiceFilter filterService = new ServiceFilter();
		filterService.deleteFilter(json.optString("badgeId"), json.optString("idFilter"));
		filterService.closeConnection();
		return new ResponseEntity<String>("Filtre supprimé !", HttpStatus.OK);
	} catch (Exception e) {
		System.out.println("ERROR WHEN DELETING FILTER : "+e);
		return new ResponseEntity<String>(ErrorProvider.BAD_MANIPULATION, HttpStatus.REQUESTED_RANGE_NOT_SATISFIABLE);
	}
	}
	
	
}
