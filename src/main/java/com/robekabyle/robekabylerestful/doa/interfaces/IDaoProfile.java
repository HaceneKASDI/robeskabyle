package com.robekabyle.robekabylerestful.doa.interfaces;

import com.robekabyle.robekabylerestful.model.Profile;

public interface IDaoProfile extends IDaoGeneric<Profile>{
	
	public Profile getProfileByUSERID(String userID);
	public Profile getProfileByUsername(String username);
	public void closeConnection();

}
