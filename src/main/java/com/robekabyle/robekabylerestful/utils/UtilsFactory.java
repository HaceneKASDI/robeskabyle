package com.robekabyle.robekabylerestful.utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

public class UtilsFactory {

	/**
	 * this method build a String of current date
	 * @return current time
	 */
	public static String getCurrentDate() {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");  
		LocalDateTime today = LocalDateTime.now();
		return dtf.format(today);
	}
	
	public static String getUUID() {
		System.out.println(UUID.randomUUID().toString());
//		return Long.toString(new Timestamp(System.currentTimeMillis()).getTime()); // SO BAD
		return UUID.randomUUID().toString();
	}  
}
