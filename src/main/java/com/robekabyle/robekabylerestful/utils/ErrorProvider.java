package com.robekabyle.robekabylerestful.utils;

/**
 * This class lists the Messages to return to the user
 * @author Acene 11-12-2018
 *
 */
public class ErrorProvider {
	
	public static final String ERROR_UPDATING_MONGODB = "Error when updating the object on mongodb";  
	public static final String ERROR_DELETING_MONGODB = "Error when deleting the object on mongodb";
	public static final String ERROR_SEARCHING_MONGODB = "Error when searching the object on mongodb";
	public static final String OK_MONGODB = "OK from mongoDB"; 
	
	public static final String USER_NOT_FOUND = "L'utilisateur n'est pas trouvé !"; 
	public static final String USER_DISCONNECTED = "L'utilisateur est déconnecté !"; 
	public static final String BAD_MANIPULATION = "Mauvaise manipulation !";
	
	public static final String COMMENT_ADDED = "Commentaire ajouté !";
	public static final String COMMENT_UPDATED = "Commentaire modifié !";
	public static final String COMMENT_DELETED = "Commentaire supprimé !";
	public static final String COMMENT_ERROR = "Une erreur est survenue, l'image n'est plus présente !";
	
	public static final String LIKE_ADDED = "Like ajouté !";
	public static final String LIKE_DELETED = "Like supprimé !";
	public static final String LIKE_ERROR = "Une erreur est survenue !";
	public static final String LIKE_EXISTS = "Contenu déja liké !";
	
	public static final String FOLLOWER_ADDED = "Abonné ajouté !";
	public static final String FOLLOWER_DELETED = "Abonné supprimé !";
	public static final String FOLLOWER_EXITS = "Abonné existe déja !";
	public static final String BADGE_ERROR = "Une erreur est survenue !";
	
	public static final String POST_IMAGE_OK = "Votre image a été publié !";
	public static final String POST_IMAGE_KO = "Image non trouvable !";
	
	public static final String REGISTER_OK = "Vous êtes parmi nous !";
	public static final String REGISTER_EXITANT = "Identifiant déja existant !";
	public static final String REGISTER_KO = "Une exception est survenue !";
}
