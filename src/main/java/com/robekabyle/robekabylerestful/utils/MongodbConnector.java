package com.robekabyle.robekabylerestful.utils;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;
import com.mongodb.ServerAddress;

/**
 * 
 * @author Hacene KASDI
 * 11/2018
 * 
 *  This class allows a connection to noSQL database, it opens a canal of connection
 *  contains a generic method witch returns <code>BDCollection</code> of mongoDB
 *   
 */
public class MongodbConnector {

	private MongoClient 	mongoClient ;
	private DB 				db;
	
	public MongodbConnector() {
		this.mongoClient = new MongoClient(new ServerAddress("localhost", 27017));
		db = mongoClient.getDB("robekabyle");
	}
	
	/**
	 * 
	 * @param collectionName : a string refers to collection name in mongoDB database
	 * @return a real collection of <code>DBCollection</code>
	 */
	public DBCollection getCollectionByName(String collectionName){
		return this.db.getCollection(collectionName);
	}
	
	public void closeConnection() {
		this.mongoClient.close();
	}
	
}
