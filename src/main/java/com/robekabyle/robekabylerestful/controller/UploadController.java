package com.robekabyle.robekabylerestful.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.net.URLDecoder;
import java.sql.Timestamp;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.robekabyle.robekabylerestful.model.Profile;
import com.robekabyle.robekabylerestful.services.implems.ServiceImages;
import com.robekabyle.robekabylerestful.services.implems.ServiceProfile;
import com.robekabyle.robekabylerestful.utils.ErrorProvider;

/**
 * This class allow us to upload images and to store it in the remote server 
 * @author Acene
 * We can stock the images provided by profile service
 * 
 */
@Controller
public class UploadController {

	/**
	 * 
	 * @param userID : the ID of the <code>Profile</code>
	 * @param file : The image
	 * @return : Message success if the updating was well done, error message if failure
	 * 
	 */
	@RequestMapping(value="/Upload/profileImage",method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE+"; charset:utf-8")
	@ResponseBody
	public ResponseEntity<String> uploadFile(@RequestParam("userID") String userID,@RequestParam("file") MultipartFile file) {

		
		if (!file.isEmpty()) {
			// Build new name for the image
			String name = file.getOriginalFilename();
			name = (new Timestamp(System.currentTimeMillis()).getTime()) + name;
			
			try {
				byte[] bytes = file.getBytes();
				
				
				String path = this.getClass().getClassLoader().getResource("").getPath();

				String fullPath = URLDecoder.decode(path, "UTF-8");
				String pathArr[] = fullPath.split("/WEB-INF/");
				System.out.println(pathArr[0]);
				

				// Creating the directory to store file
				File dir = new File(pathArr[0]+"/profiles/user-profile/"+userID);
				if (!dir.exists()) dir.mkdirs();
				System.out.println("#### "+dir.getAbsolutePath());
				
				// Create the file on server
				File serverFile = new File(dir.getAbsolutePath() + File.separator + name);
				String locationImage = serverFile.getAbsolutePath();
				BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
				stream.write(bytes);
				stream.close();
				
				// Data to store in database (image name related to current user)
				ServiceProfile serviceProfile = new ServiceProfile();
				Profile userProfile = serviceProfile.getProfileByUserID(userID);
				// On windows : userProfile.setPictureUrl(locationImage.replace('\\', '/'));
				userProfile.setPictureUrl(locationImage.replace("/var/lib/tomcat7/webapps/", ""));
				serviceProfile.updateProfile(userProfile);
				
				ServiceImages images = new ServiceImages();
				images.updateAuthorImage(userID, locationImage.replace("/var/lib/tomcat7/webapps/", ""));
				// CLOSE CONNECTIONS
				serviceProfile.closeConnection();
				
				System.out.println("Image saved for USER = "+userID+" - Location = "+ locationImage);

				return new ResponseEntity<String>("Image chargé avec succés !",HttpStatus.OK);
			} catch (Exception e) {
				System.out.println(e.toString());
				return new ResponseEntity<String>("Une erreur est survenue  !",HttpStatus.CONFLICT);
			}
		} else {
			return new ResponseEntity<String>("Aucun fichier reçu !",HttpStatus.BAD_REQUEST);
		}
	}
	
	/**
	 * 
	 * @param userID
	 * @param file
	 * @return
	 */
	@RequestMapping(value="/Upload/PostImage",method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE+"; charset:utf-8")
	@ResponseBody
	public ResponseEntity<String> uploadImagePost(@RequestParam("userID") String userID,@RequestParam("file") MultipartFile file) {

		
		if (!file.isEmpty()) {
			try {
				// CHECK IF USER EXISTS
				ServiceProfile serviceProfile = new ServiceProfile();
				Profile userProfile = serviceProfile.getProfileByUserID(userID);
		if (userProfile != null ) {

			// Build new name for the image
			String name = file.getOriginalFilename();
			name = (new Timestamp(System.currentTimeMillis()).getTime()) + name;
			
				byte[] bytes = file.getBytes();
				
				
				String path = this.getClass().getClassLoader().getResource("").getPath();

				String fullPath = URLDecoder.decode(path, "UTF-8");
				String pathArr[] = fullPath.split("/WEB-INF/");
				System.out.println(pathArr[0]);
				

				// Creating the directory to store file
				File dir = new File(pathArr[0]+"/profiles/user-profile/"+userID+"/posts");
				if (!dir.exists()) dir.mkdirs();
				
				// Create the file on server
				File serverFile = new File(dir.getAbsolutePath() + File.separator + name);
				String locationImage = serverFile.getAbsolutePath();
				BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
				stream.write(bytes);
				stream.close();

					System.out.println("Image saved for USER = "+userID+" - Location = "+ locationImage);
					locationImage = locationImage.replace("/var/lib/tomcat7/webapps/", "");
					// CLOSE CONNECTIONS
					serviceProfile.closeConnection();
					return new ResponseEntity<String>(locationImage,HttpStatus.OK);
				}else {
					// CLOSE CONNECTIONS
					serviceProfile.closeConnection();
					return new ResponseEntity<String>(ErrorProvider.USER_NOT_FOUND,HttpStatus.NOT_FOUND);
				}
			} catch (Exception e) {
				System.out.println(e.toString());
				return new ResponseEntity<String>("Une erreur est survenue  !",HttpStatus.CONFLICT);
			}
		} else {
			return new ResponseEntity<String>("Aucun fichier reçu !",HttpStatus.BAD_REQUEST);
		}
	}
	
}
