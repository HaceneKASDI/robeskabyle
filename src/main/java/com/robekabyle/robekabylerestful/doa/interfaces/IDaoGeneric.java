package com.robekabyle.robekabylerestful.doa.interfaces;

import java.util.List;

public interface IDaoGeneric<T> {

	public List<T> getAll();
	public String save(T entity);
	public String update(T entity);
	public T get(String id);
	public String delete(String id);
	public void closeConnection();
}
