package com.robekabyle.robekabylerestful.services.implems;

import java.util.List;
import java.util.stream.Collectors;

import com.robekabyle.robekabylerestful.doa.implems.DaoBadge;
import com.robekabyle.robekabylerestful.doa.interfaces.IDaoBadge;
import com.robekabyle.robekabylerestful.model.Badge;
import com.robekabyle.robekabylerestful.model.Filter;
import com.robekabyle.robekabylerestful.services.interfaces.IServiceFilter;
import com.robekabyle.robekabylerestful.utils.ErrorProvider;

public class ServiceFilter implements IServiceFilter {

	IDaoBadge daoBadge;
	
	public ServiceFilter() {
		super();
		daoBadge = new DaoBadge();
	}
	
	@Override
	public String saveNewFilter(String badgeId, Filter filter) {
		try {
			Badge badge = daoBadge.get(badgeId);
			List<Filter> filters = badge.getFilters();
			filters.add(filter);
			badge.setFilters(filters);
			daoBadge.update(badge);
			return ErrorProvider.OK_MONGODB;
		} catch (Exception e) {
			return ErrorProvider.BADGE_ERROR;
		}
	}

	@Override
	public String deleteFilter(String badgeId, String idFilter) {
		try {
			Badge badge = daoBadge.get(badgeId);
			List<Filter> filters = badge.getFilters();
			List<Filter> filtersList = filters.stream().filter(e -> (! idFilter.equals(e.getIdFilter()))).collect(Collectors.toList());
			badge.setFilters(filtersList);
			daoBadge.update(badge);
			return ErrorProvider.OK_MONGODB;
		} catch (Exception e) {
			return ErrorProvider.BADGE_ERROR;
		}
	}

	@Override
	public void closeConnection() {
		daoBadge.closeConnection();
		
	}

}
