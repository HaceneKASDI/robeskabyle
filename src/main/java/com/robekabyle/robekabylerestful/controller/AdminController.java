package com.robekabyle.robekabylerestful.controller;

import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.robekabyle.robekabylerestful.model.Profile;
import com.robekabyle.robekabylerestful.services.implems.ServiceProfile;
import com.robekabyle.robekabylerestful.utils.ErrorProvider;

@RestController
public class AdminController {
	
	@RequestMapping(value="/Admin/contactAdmin",method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE+"; charset:utf-8")
	@ResponseBody
	public String ContactAdmin(@RequestBody String data)
	{
		// data = { textMessage: "", subject: "", email: "", profileID : "", informationUser: "" }
		try {
			JSONObject json = new JSONObject(data);
			System.out.println(json.opt("textMessage"));
			System.out.println(json.opt("subject"));
			System.out.println(json.opt("email"));
			System.out.println(json.opt("profileID"));
			System.out.println(json.opt("informationUser"));
			
			MailService mailServices = new MailService();
			mailServices.sendMailRobesKabylesOrg("admin", json.optString("email"), json.optString("subject"), json.optString("textMessage")+" -- "+json.optString("informationUser"));
			return "Message envoyé";
		} catch (Exception e) {
			System.out.println("MESSAGE ADMIN KO ==> "+e.toString());
			return ErrorProvider.BAD_MANIPULATION;
		}
	}
	
	@RequestMapping(value="/Admin/recoverPassword",method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE+"; charset:utf-8")
	@ResponseBody
	public ResponseEntity<String> RecoverPassword(@RequestBody String data)
	{
		// data = {"username" : passwordRecover.username, "email" : passwordRecover.email}
		try {
			JSONObject json = new JSONObject(data);
			String username = json.opt("username").toString();
			String email = json.opt("email").toString();
			
			ServiceProfile serviceProfile = new ServiceProfile();
			Profile user = serviceProfile.getProfileByUsername(username);
			if(user.get_id() != null) {
				if(user.getEmail() != "" && email.trim().equals(user.getEmail().trim())) {
					MailService mailService = new MailService();
					
					StringBuffer textBody = new StringBuffer();
					textBody.append("Récupération de votre mot de passe : \n");
					textBody.append("Identifiant de connexion : "+username+ " \n");
					textBody.append("Mot de passe : "+user.getPassword()+ " \n");
					textBody.append(" \n");
					textBody.append(" \n");
					textBody.append("Cordialement \n");
					textBody.append("Equipe technique robes-kabyles.org \n");
					
					mailService.recoverPassword(email, "[ Récupération de votre mot de passe ]", textBody.toString());
					
					return new ResponseEntity<String>("Vous recevez un e-mail de réinitialisation de votre mot de passe", HttpStatus.RESET_CONTENT);
				}else {
					return new ResponseEntity<String>("Veuillez contacter l'administrateur ! les informations sont incorrectes", HttpStatus.MULTI_STATUS);
				}
			}else {
				return new ResponseEntity<String>("Veuillez contacter l'administrateur ! les informations sont incorrectes", HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			System.out.println("RECOVER PASSWORD KO ==> "+e.toString());
			return new ResponseEntity<String>("Une erreur est survenue ! Veuillez contacter l'administrateur", HttpStatus.BAD_REQUEST);
		}
	}

}
