package com.robekabyle.robekabylerestful.tests;

import java.util.List;

import com.robekabyle.robekabylerestful.model.ImagePost;
import com.robekabyle.robekabylerestful.services.implems.ServiceImages;

public class ServicesTester {

	public static void main(String[] args) {
		ServiceImages serviceImages = new ServiceImages();
		
		
		List<ImagePost> images = serviceImages.searchImagesByToken("ga");
		images.stream().forEach(x -> System.out.println(x.getModelDress().toString()));
	}

}
