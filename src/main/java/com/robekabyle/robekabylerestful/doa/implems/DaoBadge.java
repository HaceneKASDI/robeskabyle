package com.robekabyle.robekabylerestful.doa.implems;

import java.util.List;

import org.mongojack.JacksonDBCollection;
import org.mongojack.WriteResult;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.robekabyle.robekabylerestful.doa.DaoGeneric;
import com.robekabyle.robekabylerestful.doa.interfaces.IDaoBadge;
import com.robekabyle.robekabylerestful.model.Badge;
import com.robekabyle.robekabylerestful.utils.ErrorProvider;

public class DaoBadge 	extends DaoGeneric<Badge> 
						implements IDaoBadge {

	JacksonDBCollection<Badge, String> collection;
	public DaoBadge() {
		super();
		this.collection = super.getCollection("badge_rk", Badge.class);
	}
	
	@Override
	public List<Badge> getAll() {
		return this.collection.find().toArray();
	}

	@Override
	public String save(Badge entity) {
		WriteResult<Badge, String> result = this.collection.insert(entity);
		return result.getDbObject().get("_id").toString();
	}

	@Override
	public String update(Badge entity) {
		try {
			this.collection.updateById(entity.get_id(), entity);
			return ErrorProvider.OK_MONGODB;
		} catch (Exception e) {
			return ErrorProvider.ERROR_UPDATING_MONGODB;
		}
	}

	@Override
	public Badge get(String id) {
		return this.collection.findOneById(id);
	}

	@Override
	public String delete(String id) {
		try {
			collection.removeById(id);
			return ErrorProvider.OK_MONGODB;
		}catch (Exception e) {
			return ErrorProvider.ERROR_DELETING_MONGODB;
		}
	}

	@Override
	public void closeConnection() {
		super.closeConnection();

	}

	@Override
	public Badge getBadgeByProfile(String profileId) {
		try {
			DBObject query = new BasicDBObject("idProfile", profileId);
			return collection.findOne(query);
		} catch (Exception e) {
			return null;
		}
	}

}
