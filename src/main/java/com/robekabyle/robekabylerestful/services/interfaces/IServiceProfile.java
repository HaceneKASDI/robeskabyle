package com.robekabyle.robekabylerestful.services.interfaces;

import java.util.List;

import com.robekabyle.robekabylerestful.model.Profile;

public interface IServiceProfile {

	public List<Profile> 			getAllprofiles();
	public String 					saveNewProfile(Profile profile);
	public String 					updateProfile(Profile profile);
	public Profile		 			getProfileByID(String id);
	public Profile		 			getProfileByUserID(String userID);
	public Profile		 			getProfileByUsername(String username);
	public String					deleteProfile(String userID);
	public void						closeConnection();
}
