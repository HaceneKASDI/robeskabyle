package com.robekabyle.robekabylerestful.model;

import java.util.ArrayList;

import org.mongojack.Id;
import org.mongojack.ObjectId;

/**
 * This class represents the Post image which contains identity of the author,
 * the image url in the server, the text of the share and list of likes that represents
 * the list of idProfile of users and list of comments <code>PostComments</code>
 *  
 * @author Acene
 * @version 14-12-2018
 */

public class ImagePost {

	@Id @ObjectId
	private String _id;
	private String idProfile;
	private String urlImagePost;
	private String textPost;
	private String fullName;
	private String urlProfile;
	private String dateShare;
	private PostModel modelDress;
	private ArrayList<String> likes;
	private ArrayList<PostComments> comments;

	public ImagePost() {
		super();
	}

	public ImagePost(String idProfile, String urlImagePost, String textPost, String fullName, String urlProfile,
			String dateShare, PostModel modelDress) {
		super();
		this.idProfile = idProfile;
		this.urlImagePost = urlImagePost;
		this.textPost = textPost;
		this.fullName = fullName;
		this.urlProfile = urlProfile;
		this.dateShare = dateShare;
		this.modelDress = modelDress;
		comments = new ArrayList<PostComments>();
		likes = new ArrayList<String>();
	}

	public ImagePost(String idProfile, String urlImagePost, String textPost, ArrayList<String> likes,
			ArrayList<PostComments> comments) {
		super();
		this.idProfile = idProfile;
		this.urlImagePost = urlImagePost;
		this.textPost = textPost;
		this.likes = likes;
		this.comments = comments;
	}

	
	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	public String getIdProfile() {
		return idProfile;
	}

	public void setIdProfile(String idProfile) {
		this.idProfile = idProfile;
	}

	public String getUrlImagePost() {
		return urlImagePost;
	}

	public void setUrlImagePost(String urlImagePost) {
		this.urlImagePost = urlImagePost;
	}

	public String getTextPost() {
		return textPost;
	}

	public void setTextPost(String textPost) {
		this.textPost = textPost;
	}

	public ArrayList<String> getLikes() {
		return likes;
	}

	public void setLikes(ArrayList<String> likes) {
		this.likes = likes;
	}

	public ArrayList<PostComments> getComments() {
		return comments;
	}

	public void setComments(ArrayList<PostComments> comments) {
		this.comments = comments;
	}

	public PostModel getModelDress() {
		return modelDress;
	}

	public void setModelDress(PostModel modelDress) {
		this.modelDress = modelDress;
	}


	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getUrlProfile() {
		return urlProfile;
	}

	public void setUrlProfile(String urlProfile) {
		this.urlProfile = urlProfile;
	}

	public String getDateShare() {
		return dateShare;
	}

	public void setDateShare(String dateShare) {
		this.dateShare = dateShare;
	}

	
	@Override
	public String toString() {
		return "ImagePost [_id=" + _id + ", idProfile=" + idProfile + ", urlImagePost=" + urlImagePost + ", textPost="
				+ textPost + "]";
	}

	

	
}
