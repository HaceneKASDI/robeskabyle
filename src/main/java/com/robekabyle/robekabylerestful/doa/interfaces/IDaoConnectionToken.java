package com.robekabyle.robekabylerestful.doa.interfaces;

import com.robekabyle.robekabylerestful.model.ConnectionToken;

public interface IDaoConnectionToken extends IDaoGeneric<ConnectionToken> {
	
	public ConnectionToken findConnectionByUsername(String username);
	public ConnectionToken findConnectionByFacebookID(String facebookID);
	public ConnectionToken findAutheticatedUser(String username, String password);
	public boolean IsAuthenticated(String profileID);
	public void closeConnection();

}
