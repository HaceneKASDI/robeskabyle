package com.robekabyle.robekabylerestful.services.implems;

import java.util.List;

import com.robekabyle.robekabylerestful.doa.implems.DaoBadge;
import com.robekabyle.robekabylerestful.doa.interfaces.IDaoBadge;
import com.robekabyle.robekabylerestful.model.Badge;
import com.robekabyle.robekabylerestful.services.interfaces.IServiceBadge;
import com.robekabyle.robekabylerestful.utils.ErrorProvider;

public class ServiceBadge implements IServiceBadge {

	IDaoBadge daoBadge;
	public ServiceBadge() {
		super();
		daoBadge = new DaoBadge();
	}
	
	@Override
	public List<Badge> getAllBadges() {
		return daoBadge.getAll();
	}

	@Override
	public String saveNewBadge(Badge badge) {
		return daoBadge.save(badge);
	}

	@Override
	public String updateBadge(Badge badge) {
		System.out.println("UPDATING BADGE USER === "+badge.getIdProfile());
		return daoBadge.update(badge);
	}

	@Override
	public Badge getBadgeByID(String id) {
		return daoBadge.get(id);
	}

	@Override
	public Badge getProfileByUserID(String userID) {
		return daoBadge.getBadgeByProfile(userID);
	}

	@Override
	public String deleteBadge(String badgeID) {
		return daoBadge.delete(badgeID);
	}

	@Override
	public void closeConnection() {
		daoBadge.closeConnection();
	}

	@Override
	public String saveFollower(String badgeID, String follower) {
		try {
			Badge badge = daoBadge.get(badgeID);
			if(! badge.getFollowers().contains(follower)) {
				List<String> followers = badge.getFollowers();
				followers.add(follower);				
				daoBadge.update(badge);
				return ErrorProvider.FOLLOWER_ADDED;
			}
			return ErrorProvider.FOLLOWER_EXITS;
		} catch (Exception e) {
			return ErrorProvider.BADGE_ERROR;
		}
	}

	@Override
	public String deleteFollower(String badgeID, String follower) {
		try {
			Badge badge = daoBadge.get(badgeID);
			if( badge.getFollowers().contains(follower)) {
				List<String> followers = badge.getFollowers();
				followers.remove(follower);
				daoBadge.update(badge);
				return ErrorProvider.FOLLOWER_DELETED;
			}
			return ErrorProvider.USER_NOT_FOUND;
		} catch (Exception e) {
			return ErrorProvider.BADGE_ERROR;
		}
	}

}
