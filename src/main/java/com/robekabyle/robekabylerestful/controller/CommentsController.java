package com.robekabyle.robekabylerestful.controller;

import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.robekabyle.robekabylerestful.model.ImagePost;
import com.robekabyle.robekabylerestful.model.PostComments;
import com.robekabyle.robekabylerestful.model.Profile;
import com.robekabyle.robekabylerestful.services.implems.ServiceConnection;
import com.robekabyle.robekabylerestful.services.implems.ServiceImages;
import com.robekabyle.robekabylerestful.services.implems.ServiceProfile;
import com.robekabyle.robekabylerestful.utils.ErrorProvider;

/**
 * This class allows Comments crud operations
 * we can addComment(imageId,Comment), updateComment(imageId,Comment), 
 * deleteComment(imageId,commentID, profileID)
 * @author Acene 18-12-2018
 * @version 2018
 * 
 */
@RestController
public class CommentsController {

	/**
	 * This method called in HTTP POST to add new Comment to the ImageProfile
	 * @param imageID : the Id of the <code>ImagePost</code>
	 * @param comment : the <code>PostComment</code> to add to the image
	 * @return HTTP Status 200 if all is okay 
	 * 				Status 404 if user not found and not connected
	 * 				Status 416 if an exception was occurred
	 */
	@RequestMapping(value="/PostComment/addComment",method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE+"; charset:utf-8")
	@ResponseBody
	public ResponseEntity<String> addComment(@RequestParam String imageID, @RequestBody PostComments comment)
	{
		try {
			// CHECK IF IMAGE EXITS
			ServiceImages serviceImages = new ServiceImages();
			if (serviceImages.getImageByID(imageID).get_id() != null) {
				
				// CHECK IF USER EXISTS AND CONNECTED
				ServiceConnection serviceConnect = new ServiceConnection();
				if(serviceConnect.isUserConnected(comment.getIdProfile())) {
				ServiceProfile servPro = new ServiceProfile();
				Profile profile = servPro.getProfileByUserID(comment.getIdProfile());
				String fullName = profile.getPrenom()+" "+profile.getNom();
				comment.setFullName(fullName);
				comment.setProfileImage(profile.getPictureUrl());
				
				//ADD COMMENT TO THE RELATED IMAGE
				serviceImages.addNewComment(imageID, comment);
				// CLOSE CONNECTIONS
				serviceConnect.closeConnection();
				serviceImages.closeConnection();
				servPro.closeConnection();
				return new ResponseEntity<String>(ErrorProvider.COMMENT_ADDED, HttpStatus.OK);
				}else {
					return new ResponseEntity<String>(ErrorProvider.USER_DISCONNECTED, HttpStatus.NOT_FOUND);
				}
			}else {
				return new ResponseEntity<String>(ErrorProvider.COMMENT_ERROR, HttpStatus.NOT_FOUND);				
			}
			
		} catch (Exception e) {
			return new ResponseEntity<String>(ErrorProvider.BAD_MANIPULATION, HttpStatus.REQUESTED_RANGE_NOT_SATISFIABLE);
		}
	}
	
	/**
	 * Method allows updating of the comment by the author only /!\
	 * this method check if the comment in question trying to be updated by the 
	 * same author - TO OPTIMATE
	 * @param imageID
	 * @param comment
	 * @return
	 */
	@RequestMapping(value="/PostComment/updateComment",method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE+"; charset:utf-8")
	@ResponseBody
	public ResponseEntity<String> updateComment(@RequestParam String imageID, @RequestBody PostComments comment)
	{
		try {
			// CHECK IF IMAGE EXITS
			ServiceImages serviceImages = new ServiceImages();
			if (serviceImages.getImageByID(imageID).get_id() != null) {
				
				// CHECK IF USER EXISTS AND CONNECTED
				ServiceConnection serviceConnect = new ServiceConnection();
				if(serviceConnect.isUserConnected(comment.getIdProfile())) {
					
				//UPDATE COMMENT RELATED TO IMAGE
				serviceImages.updateComment(imageID, comment);
				
				// CLOSE CONNECTIONS
				serviceConnect.closeConnection();
				serviceImages.closeConnection();
				return new ResponseEntity<String>(ErrorProvider.COMMENT_UPDATED, HttpStatus.OK);
				}else {
					return new ResponseEntity<String>(ErrorProvider.USER_DISCONNECTED, HttpStatus.NOT_FOUND);
				}
			}else {
				return new ResponseEntity<String>(ErrorProvider.COMMENT_ERROR, HttpStatus.NOT_FOUND);				
			}
			
		} catch (Exception e) {
			return new ResponseEntity<String>(ErrorProvider.BAD_MANIPULATION, HttpStatus.REQUESTED_RANGE_NOT_SATISFIABLE);
		}
	}
	
	
	@RequestMapping(value="/PostComment/deleteComment",method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE+"; charset:utf-8")
	@ResponseBody
	public ResponseEntity<String> deleteComment(@RequestParam String imageID, @RequestBody String data)
	{
		try {
			// CHECK IF IMAGE EXITS
			ServiceImages serviceImages = new ServiceImages();
			ImagePost image= serviceImages.getImageByID(imageID);
			if (image.get_id() != null) {
				
				/** data modal {"commentID": "", "userID" : ""} */
				JSONObject json = new JSONObject(data);
				// CHECK IF USER EXISTS AND CONNECTED
				ServiceConnection serviceConnect = new ServiceConnection();
				if(serviceConnect.isUserConnected(json.optString("userID"))) {
					
				//DELETE COMMENT RELATED TO IMAGE
				serviceImages.deleteComment(imageID, json.optString("commentID"));
				
				// CLOSE CONNECTIONS
				serviceConnect.closeConnection();
				serviceImages.closeConnection();
				return new ResponseEntity<String>(ErrorProvider.COMMENT_DELETED, HttpStatus.OK);
				}else {
					return new ResponseEntity<String>(ErrorProvider.USER_DISCONNECTED, HttpStatus.NOT_FOUND);
				}
			}else {
				return new ResponseEntity<String>(ErrorProvider.COMMENT_ERROR, HttpStatus.NOT_FOUND);				
			}
			
		} catch (Exception e) {
			return new ResponseEntity<String>(ErrorProvider.BAD_MANIPULATION, HttpStatus.REQUESTED_RANGE_NOT_SATISFIABLE);
		}
	}
	
}
