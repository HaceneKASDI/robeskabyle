package com.robekabyle.robekabylerestful.controller;

import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.robekabyle.robekabylerestful.model.Badge;
import com.robekabyle.robekabylerestful.model.ConnectionToken;
import com.robekabyle.robekabylerestful.model.Profile;
import com.robekabyle.robekabylerestful.services.implems.ServiceBadge;
import com.robekabyle.robekabylerestful.services.implems.ServiceConnection;
import com.robekabyle.robekabylerestful.services.implems.ServiceProfile;
import com.robekabyle.robekabylerestful.utils.ErrorProvider;
import com.robekabyle.robekabylerestful.utils.UtilsFactory;

/**
 * This class allow us to answer for LOGIN and SUBSCRIPTION services 
 * @author Acene
 * we can invoke LOGIN service to have an access to the whole services by the ordinary LOGIN or using Facebook
 * we can LOGOUT from services and/or REGISTER  
 */
@RestController
public class LoginController {

	@RequestMapping("/azul")
	@ResponseBody
	public String welcome()
	{
		return "ANSUF YISWEN AR AZETTA ANMTTI Robes Kabyle 2018";
	}
	
	@RequestMapping("/")
	@ResponseBody
	public String azul()
	{
		return "ANSUF YISWEN AR AZETTA ANMTTI ";
	}
	
	
	/**
	 * The method allows user to LOGIN
	 * @param userConnection : Token to connect to the application
	 * @return : success token if all is write, empty token else
	 */
	@RequestMapping(value="/login",method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE+"; charset:utf-8")
	@ResponseBody
	public ResponseEntity<ConnectionToken> loginUser(@RequestBody ConnectionToken userConnection)
	{
		System.out.println(userConnection.getUsername()+" === "+userConnection.getPassword());
		try {
			ServiceConnection serviceConnect = new ServiceConnection();
			ConnectionToken connectionFound = serviceConnect.authenticateUser(userConnection.getUsername(), userConnection.getPassword());
			if (connectionFound == null ) {
				ConnectionToken connectionNotFound = new ConnectionToken();
				connectionNotFound.setObservation("Identifiant ou mot de passe est incorrect !");
				System.out.println(connectionNotFound.getObservation());
				
				// CLOSE CONNECTIONS
				serviceConnect.closeConnection();
				return new ResponseEntity<ConnectionToken>(connectionNotFound,HttpStatus.OK);
			}else {
				connectionFound.setObservation("Préparation de votre compte...");
				connectionFound.setConnected(true);
				serviceConnect.updateConnection(connectionFound);
				System.out.println(connectionFound.toString());
				
				// CLOSE CONNECTIONS
				serviceConnect.closeConnection();
				return new ResponseEntity<ConnectionToken>(connectionFound,HttpStatus.OK);
			}			
		} catch (Exception e) {
			ConnectionToken connectionNotFound = new ConnectionToken();
			connectionNotFound.setObservation("Erreur du serveur. Patientez un instant..."+e);
			System.out.println(connectionNotFound.getObservation());
			return new ResponseEntity<ConnectionToken>(connectionNotFound,HttpStatus.NOT_FOUND);
		}
	}
	
	/**
	 * 
	 * @param profileUser : the informations collected from graph facebook
	 * @return : success token if all is write, empty token else
	 */
	@RequestMapping(value="/login/withfacebook",method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE+"; charset:utf-8")
	@ResponseBody
	public ConnectionToken loginWithFacebook(@RequestBody Profile profileUser)
	{
		ServiceConnection serviceConnect = new ServiceConnection();
		ConnectionToken userLoginWithFacebook = serviceConnect.getConnectionByFacebookID(profileUser.getFacebookID());
		if(userLoginWithFacebook == null) {
			ServiceProfile serviceProfile = new ServiceProfile();
			String idUser = serviceProfile.saveNewProfile(profileUser);
			 userLoginWithFacebook = new ConnectionToken(idUser, "", "", UtilsFactory.getCurrentDate(), true, profileUser.getFacebookID(),"");
			serviceConnect.saveNewConnection(userLoginWithFacebook);
			
			// CLOSE CONNECTIONS
			serviceConnect.closeConnection();
			serviceProfile.closeConnection();
		}
		return userLoginWithFacebook;
	}
	
	/**
	 * This method allows 
	 * @param profileUser : The user <code>Profile</code> to save in the database
	 * @return : success message if all is ok, error message else
	 */
	@RequestMapping(value="/register",method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE+"; charset:utf-8")
	@ResponseBody
	public ResponseEntity<String> registerUser(@RequestBody Profile profileUser)
	{
		try {
			
			System.out.println("REGISTER : "+profileUser.getUsername()+" "+profileUser.getNom());
			// 0. we have to check if there is no double entry
			ServiceProfile serviceProfile = new ServiceProfile();
			Profile profileExisting = serviceProfile.getProfileByUsername(profileUser.getUsername());
			if (profileExisting == null) {
				// 1. we have to save the user Profile
				String idUser = serviceProfile.saveNewProfile(profileUser);
				// 2. we have to create instance of Connection with isConnected = false
				ServiceConnection serviceConnect = new ServiceConnection();
				ConnectionToken newUser = new ConnectionToken(idUser, profileUser.getUsername(), profileUser.getPassword(), UtilsFactory.getCurrentDate(), false, "","");
				serviceConnect.saveNewConnection(newUser);
				
				//ADD NEW BADGE FOR THE REGISTRED USER
				ServiceBadge serviceBadge = new ServiceBadge();
				Badge badge = new Badge(idUser, 1, "", "", "", "");
				serviceBadge.saveNewBadge(badge);
				
				// CLOSE CONNECTIONS
				serviceBadge.closeConnection();
				serviceConnect.closeConnection();
				serviceProfile.closeConnection();
				return new ResponseEntity<String>(ErrorProvider.REGISTER_OK,HttpStatus.OK);	
			} else {
				// CLOSE CONNECTIONS
				serviceProfile.closeConnection();
				return new ResponseEntity<String>(ErrorProvider.REGISTER_EXITANT,HttpStatus.CONFLICT);
			}
		} catch (Exception e) {
			System.out.println("REGISTER KO ==> "+e.toString());
			return new ResponseEntity<String>(ErrorProvider.REGISTER_KO,HttpStatus.NOT_FOUND);	
		}
	}
	
	/**
	 * 
	 * @param userConnection : the connected User
	 * @return : the user <code>Profile</code> if esxits
	 */
	@RequestMapping(value="/profileUser",method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE+"; charset:utf-8")
	@ResponseBody
	public Profile profileBuUserID(@RequestBody ConnectionToken userConnection)
	{
		try {
			ServiceProfile serviceProfile = new ServiceProfile();
			Profile user = serviceProfile.getProfileByUserID(userConnection.getUserId());
			// CLOSE CONNECTIONS
			serviceProfile.closeConnection();
			System.out.println(user.toString());
			return user;			
		} catch (Exception e) {
			System.out.println("PROFILE USER KO ==> "+e.toString());
			return new Profile();
		}
	}
	
	
	@RequestMapping(value="/logoutUser",method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE+"; charset:utf-8")
	@ResponseBody
	public String deconnecteUser(@RequestBody String username)
	{
		try {
			ServiceConnection connectionToken = new ServiceConnection();
			JSONObject json = new JSONObject(username);
			ConnectionToken userConnection = connectionToken.getConnectionByUsername(json.optString("username"));
			connectionToken.logoutUser(userConnection);
			
			// CLOSE CONNECTIONS
			connectionToken.closeConnection();
			return ErrorProvider.USER_DISCONNECTED;
		} catch (Exception e) {
			System.out.println("PROFILE USER KO ==> "+e.toString());
			return ErrorProvider.BAD_MANIPULATION;
		}
	}
}
