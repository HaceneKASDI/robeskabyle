package com.robekabyle.robekabylerestful.tests;

import java.util.ArrayList;
import java.util.List;

import com.robekabyle.robekabylerestful.model.ImagePost;
import com.robekabyle.robekabylerestful.model.PostComments;
import com.robekabyle.robekabylerestful.services.implems.ServiceImages;
import com.robekabyle.robekabylerestful.utils.UtilsFactory;

public class ServiceTesterImages {
	public static void main(String[] args) {
		
		ServiceImages serviceImages = new ServiceImages();
//		ArrayList<PostComments> listComments = new ArrayList<PostComments>();
		PostComments comment1 = new PostComments("XX5", "ça c'est un comment XX", "14-12-2018");
//		PostComments comment2 = new PostComments(UtilsFactory.getUUID(),"PROF2", "ça c'est un comment 2", "14-12-2018");
//		PostComments comment3 = new PostComments(UtilsFactory.getUUID(),"PROF3", "ça c'est un comment 3", "14-12-2018");
//		
//		listComments.add(comment1);
//		listComments.add(comment2);
//		listComments.add(comment3);
//			
//		
//		ImagePost image = new ImagePost(
//				"65TY87UOJSK", 
//				"http://iouazoiue.png", 
//				"Cette phot represente la première image dans la base", 
//				null, 
//				listComments);
//		// ADD THE IMAGE TO MONGODB DATABASE
//		String response = serviceImages.saveNewImage(image);
//		System.out.println(response);
//		
//		ImagePost imageRecovred = serviceImages.getImageByID(response);
//		System.out.println(imageRecovred.getComments().toString());
		
		
//		ImagePost image = new ImagePost(
//				"65TY87UOJSK", 
//				"http://iouazoiue.png", 
//				"Cette phot represente la première image dans la base");
//		String idImage = serviceImages.saveNewImage(image);
		
		// UPDATE IMAGE COLLECTION
		// 5c13d5c4df147025a8b9e321
		ImagePost imageReco = serviceImages.getImageByID("5c13d5c4df147025a8b9e321");
		System.out.println(imageReco.toString());
		
		ArrayList<PostComments> listComments = imageReco.getComments();
		listComments.add(comment1);
		imageReco.setComments(listComments);
		serviceImages.updateImage(imageReco);
		
		ImagePost imageR = serviceImages.getImageByID("5c13d5c4df147025a8b9e321");
		System.out.println(imageR.toString());
	}

}
